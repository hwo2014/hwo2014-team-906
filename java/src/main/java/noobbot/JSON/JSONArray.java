package noobbot.JSON;


import com.google.common.base.Joiner;

import java.util.*;

public class JSONArray extends ArrayList<Object> {

    public JSONArray() {

    }

    public JSONArray(Collection<?> collection) {
        for (Object o : collection) {
            this.add(o);
        }
    }

    public int length() {
        return this.size();
    }

    public JSONObject getJSONObject(int index) {
        return (JSONObject) this.get(index);
    }

    public String getString(int index) {
        return (String) this.get(index);
    }

    public JSONArray(List< Map> list) {
        for (Map map : list) {
            this.add( JSONObject.toJSONObject(new JSONObject(), map));
        }
    }

    public JSONArray put(Object object) {
        super.add(object);
        return this;
    }

    public JSONArray put(int index, Object object) {
        super.add(index, object);
        return this;
    }

    public JSONArray addToArrayIfNotExisting(JSONArray jsonArray) {
        for (Object object : jsonArray) {
            if (!this.contains(object)) {
                super.add(object);
            }
        }
        return this;
    }

    public JSONObject findFirstBy(String key, Object value) {
        for (Object object : asList()) {
            if (((JSONObject) object).getObject(key).toString().equals(value.toString())) {
                return ((JSONObject) object);
            }
        }
        return null;
    }

    public JSONArray addToArray(JSONArray jsonArray) {
        this.addAll(jsonArray);
        return this;
    }

    @SuppressWarnings("unchecked")
    public <T> List<T> asList() {
        ArrayList<T> list = new ArrayList<T>();
        for (int i = 0; i < length(); ++i) {
            list.add((T) get(i));
        }
        return list;
    }

    public <T> Iterator<T> asIterator() {
        return new Iterator<T>() {
            int currentIndex = 0;

            @Override
            public boolean hasNext() {
                return currentIndex < length();
            }

            @Override
            public T next() {
                T element = (T) get(currentIndex);
                currentIndex++;
                return element;
            }

            @Override
            public void remove() {
                throw new UnsupportedOperationException("remove not supported");
            }
        };
    }


    public String join(String separator) {
        return Joiner.on(separator).join(asList());
    }
}
