package noobbot.JSON;



public abstract class JsonException extends RuntimeException {

  public JsonException(String message) {
    super(message);
  }

  public JsonException(String message, Throwable error) {
    super(message, error);
  }

  public JsonException(Throwable e) {
    super(e);
  }

  public static class Access extends JsonException {
    public Access(Throwable e) {
      super(e);
    }
    
    public Access(String message) {
      super(message);
    }
  }
  
  public static class Path extends JsonException {
    public Path(Throwable e) {
      super(e);
    }
    
    public Path(String message) {
      super(message);
    }
  }
  
  public static class Conversion extends JsonException {
    public Conversion(Throwable e) {
      super(e);
    }
    
    public Conversion(String message) {
      super(message);
    }

    public Conversion(String message, Throwable e) {
        super(message, e);
    }
  }
}