package noobbot.JSON;


import com.google.common.base.Optional;
import com.google.gson.Gson;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public final class JSONObject extends java.util.LinkedHashMap<java.lang.String,java.lang.Object> {
    public JSONObject() {
    }

    private Gson getFactory() {
        return new Gson()  ;
    }

    public JSONObject(String str) {
         toJSONObject(this, (LinkedHashMap)getFactory().fromJson(str, LinkedHashMap.class));
    }

    public String toString() {
        return getFactory().toJson(this);
    }



    public JSONObject(Map<String, ?> m) {
        toJSONObject(this, m);
    }

    public JSONObject(JSONObject jsonObject) {
        for (String key : jsonObject.keySet()) {
            Object value = jsonObject.get(key);
            put(key, value);
        }
    }

    public Object ensureNotNull(String key, Object value) {
        if (value != null) {
            return value;
        }
        throw new JsonException.Access("Can not access key " + key);
    }

    public JSONObject getJSONObject(String key) {
        Object obj = get(key);
        ensureNotNull(key, this.get(key));
        if (obj instanceof JSONObject) {
            return (JSONObject) obj;
        }
        throw new JsonException.Access("Can not access to key " + key);
    }

    public JSONObject getJSONObjectWithPath(String path) {
        return (JSONObject)getObject(path);
    }

    public JSONArray getJSONArrayWithPath(String path) {
        return (JSONArray)getObject(path);
    }

    public String getStringWithPath(String path) {
        return (String) getObject(path);
    }

    public String getString(String key) {
        return ensureNotNull(key, get(key)).toString();
    }

    public String getStringWithDefault(String path, String defaultValue) {
        return (String) getObject(path, defaultValue);
    }

    public int getInt(String key) {
        return (Integer) ensureNotNull(key, get(key));
    }

    public JSONArray getJSONArray(String key) {
        Object obj = get(key);
        if (!(obj instanceof JSONArray)) {
            throw new JsonException.Access("Can not access to key " + key);
        }
        return (JSONArray) obj;

    }
    
    public boolean isJSONArray(String key) {
        Object obj = get(key);
        return obj instanceof JSONArray;
    }

    public Iterator<String> keys() {
        return keySet().iterator();
    }

    public Collection<String> getNames() {
        return keySet();
    }

    public int length() {
        return this.size();
    }

    public boolean has(String fieldName) {
        return this.containsKey(fieldName);
    }

    public JSONObject put(String key, Object value) {
        super.put(key, value);
        return this;
    }

    public JSONObject put(String key, JSONArray value) {
        super.put(key, value);
        return this;
    }

    public JSONObject put(String key, Collection<?> value) {
        JSONArray jsonArray = new JSONArray(value);
        super.put(key, jsonArray);
        return this;
    }




    public boolean contains(String path) {
        // Optimization: if there is no array in the path then do a quicker check
        if (!path.contains("[")) {
            return containsForPathsWithoutArrays(path);

        } else {
            try {
                getObject(path);
                return true;
            } catch (JsonException.Path e) {
                // exception was raised because there was no object -> ignore it
                return false;
            }
        }
    }

    // If the path does not contain any array references there is a quick way to check the existence of path
    // DOES NOT WORK ON PATHS THAT HAVE ARRAYS and it does NOT test for this (this is performance optimization after all)!!!!
    boolean containsForPathsWithoutArrays(String path) {
        if (path.startsWith(".")) {
            path = path.substring(1);
        }
        String[] parts = path.split("[\\.\\[\\]]");
        Object currentObject = this;
        if (path.equals("") || this.get(path) != null) {
            return true;
        } else {
            for (int i=0; i < parts.length-1; i++) {
                currentObject = ((JSONObject)currentObject).get(parts[i]);
                if (currentObject == null || !(currentObject instanceof JSONObject)) {
                    return false;
                }
            }

            return ((JSONObject)currentObject).has(parts[parts.length-1]);
        }
    }

    public Object getObject(String path) {
        if (path.startsWith(".")) {
            path = path.substring(1);
        }

        Object o = get(path);
        return (o != null) ? o : walk(path);
    }

    public Double getAsDouble(String path) {
        if (path.startsWith(".")) {
            path = path.substring(1);
        }
        return convertToDouble(getObject(path).toString());
    }
    
    public Boolean getAsBoolean(String path) {
        if (path.startsWith(".")) {
            path = path.substring(1);
        }
        return convertToBoolean(getObject(path).toString());
    }

    public Integer getAsInt(String path) {
        if (path.startsWith(".")) {
            path = path.substring(1);
        }
        if (this.contains(path)) {
            return convertToInt(getObject(path).toString());
        } else {
            throw new JsonException.Path("Error when extracting json object as integer");
        }
    }

    public Optional<String> getOptionalStringWithPath(String path) {
        return getOptionalTypeWithPath(path, String.class);
    }

    public Optional<Date> getOptionalDateWithPath(String path) {
        return getOptionalTypeWithPath(path, Date.class);
    }

    public Optional<Boolean> getOptionalBooleanWithPath(String path) {
        return getOptionalTypeWithPath(path, Boolean.class);
    }
    
    public Optional<JSONObject> getOptionalJSONObjectWithPath(String path) {
        return getOptionalTypeWithPath(path, JSONObject.class);
    }
    
    public Optional<JSONArray> getOptionalJSONArrayWithPath(String path) {
        return getOptionalTypeWithPath(path, JSONArray.class);
    }
    
    public <T> Optional<T> getOptionalTypeWithPath(String path, Class<T> type) {
        if (path.startsWith(".")) {
            path = path.substring(1);
        }
        if (this.contains(path)) {
            return Optional.of(type.cast(getObject(path)));
        } else {
            return Optional.absent();
        }
    }

    public Object getObject(String path, Object defaultReturnObject) {
        try {
            Object value = getObject(path);
            if (value == null) {
                return defaultReturnObject;
            }
            return value;
        } catch (Exception e) {
            return defaultReturnObject;
        }
    }

    public Object getObjectOrSetDefault(String path, Object defaultReturnObject) {
        try {
            Object value = getObject(path);
            if (value == null) {
                setObject(path, defaultReturnObject);
                return defaultReturnObject;
            }
            return value;
        } catch (Exception e) {
            setObject(path, defaultReturnObject);
            return defaultReturnObject;
        }
    }

    /**
     * Set a value to the path
     *
     * @param path  nested path to the object. The path is like  key[index1][index2].key2[index3] ...
     * @param value value of path
     * @return return json object
     */
    public JSONObject setObject(String path, Object value) {
        if (path.startsWith(".")) {
            path = path.substring(1);
        }
        if (isPathNested(path)) {
            String firstNodeName = path.substring(0, path.indexOf('.'));
            JSONObject firstNode = ensureThereIsKey(firstNodeName);
            firstNode.setObject(path.substring(path.indexOf('.') + 1), value);
        } else {
            putToPath(path, value);
        }
        return this;
    }

    public JSONObject setObjectWithNotNullValue(String path, Object value) {
        if (value != null) {
            setObject(path, value);
        }
        return this;
    }






    public static class JSONObjectOperation {
        public Object operateOn(String key, Object o) {
            return o;
        }

        public Object operateOnBasicTypeJSONArrayValue(Object o) {
            return o;
        }
    }





    private void renameKey(JSONObject parent, String oldChildName, String newChildName) {
        Object toBeRenamed = parent.get(oldChildName);
        if (toBeRenamed != null) {
            parent.remove(oldChildName);
            parent.put(newChildName, toBeRenamed);
        }
    }

    public static boolean isPathNested(String path) {
        return path.contains(".");
    }

    /**
     * If the key is not existing, create a new key with empty JSONObject
     *
     * @param key the key to be checked
     * @return the corresponding JSONObject of  key
     */
    private JSONObject ensureThereIsKey(String key) {
        JSONObject firstNode;
        try {
            firstNode = (JSONObject) walk(key);
        } catch (JsonException.Path e) {
            firstNode = new JSONObject();
            putToPath(key, firstNode);
        }
        return firstNode;
    }

    private Matcher getArrayMatcher(String path) {
        final String arrayRegex = "(\\w*)\\[((\\d)+)\\]";
        Pattern patternArray = Pattern.compile(arrayRegex);
        return patternArray.matcher(path);
    }

    /**
     * @param path  The path is like key or key[index1][index2]
     * @param value The value of the path
     */
    private void putToPath(String path, Object value) {
        Matcher m = getArrayMatcher(path);
        if (m.find()) {
            int index = Integer.parseInt(m.group(2));
            JSONArray parent;
            try {
                parent = getJSONArray(m.group(1));
            } catch (JsonException.Access e) {
                parent = new JSONArray();
                put(m.group(1), parent);
            }
            while (m.find()) {
                Object obj = parent.get(index);
                if (obj == null) {
                    obj = new JSONArray();
                    parent.put(index, obj);
                }
                index = Integer.parseInt(m.group(2));
                parent = (JSONArray) obj;
            }
            parent.put(index, value);
        } else {
            put(path, value);
        }

    }

    public JSONObject merge(JSONObject source) {
        for(String key: source.keySet()) {
            put(key, source.get(key));
        }
        return this;
    }

    public static String lastNodeInPath(String path) {
        if (path.lastIndexOf('.') > 0) {
            return path.substring(path.lastIndexOf('.') + 1);
        } else {
            return path;
        }
    }

    public static String firstNodeInPath(String path) {
        if (path.lastIndexOf('.') > 0) {
            String key = path.substring(0, path.indexOf('.'));
            if (key.indexOf('[') >= 0) {
                key = key.substring(0, key.indexOf('['));
            }
            return key;
        } else {
            return path;
        }
    }

    public static String parentPath(String path) {
        if (path.lastIndexOf('.') > 0) {
            return path.substring(0, path.lastIndexOf('.'));
        } else {
            return "";
        }
    }

    public static String pathWithoutFirstNode(String currentRemainingPath) {
        if (currentRemainingPath.contains(".")) {
            return currentRemainingPath.substring(currentRemainingPath.indexOf('.') + 1);
        } else {
            return "";
        }
    }

    private Object walk(String path) {
        List<PathComponent> pathComponents = parsePath(path);
        Object value = this;
        for (JSONObject.PathComponent component : pathComponents) {
            value = component.walk(value);
            if (value == null) {
                return null;
            }
        }
        return value;
    }

    private static boolean isNumeric(String str) {
        if (str != null && !str.isEmpty() && Character.isDigit(str.charAt(str.length()-1))) {
            try
            {
                Integer.parseInt(str);
                return true;
            }
            catch(NumberFormatException nfe)
            {
                return false;
            }
        } else {
            return false;
        }
    }

    private static boolean isDoubleOrInteger(String s) {
        try {
            Double.parseDouble(s);
            return true;
        } catch(NumberFormatException e) {
            return false;
        }
    }

    private Double convertToDouble(String s) {
        if(isDoubleOrInteger(s)) {
            return Double.parseDouble(s);
        } else {
            throw new ClassCastException("Cannot cast " + s + " to double.");
        }
    }

    private Boolean convertToBoolean(String s) {
        return Boolean.parseBoolean(s);
    }

    private Integer convertToInt(String s) {
        if(isNumeric(s)) {
            return Integer.parseInt(s);
        } else {
            throw new ClassCastException("Cannot cast " + s + " to int.");
        }
    }

    private static List<PathComponent> parsePath(String path) {
        ArrayList<PathComponent> components = new ArrayList<PathComponent>();
        String[] parts = path.split("[\\.\\[\\]]");
        StringBuilder pathSoFar = new StringBuilder("<root>");
        for (String part : parts) {
            if (part != null && !part.equals("")) {  // skip. This occurs in [0][1], for example.
                if (JSONObject.isNumeric(part)) {
                    int index = Integer.parseInt(part);
                    components.add(new JSONObject.IndexedComponent(index, pathSoFar.toString()));
                    pathSoFar.append("[").append(index).append("]");
                } else {
                    components.add(new JSONObject.NamedComponent(part, pathSoFar.toString()));
                    pathSoFar.append(".").append(part);
                }
            }
        }
        return components;
    }

    @SuppressWarnings("unchecked")
    public static JSONObject toJSONObject(JSONObject jsonObject, Map<String, ?> o) {
        for (Map.Entry<String, ?> me : o.entrySet()) {
            Object v = me.getValue();
            if (v instanceof List) {
                jsonObject.put(me.getKey(), toJSONArray((List<?>) v));
            } else if (v instanceof AbstractMap) {
                AbstractMap<String, ?> hashMap = (AbstractMap<String, ?>) v;

                    jsonObject.put(me.getKey(), toJSONObject(hashMap));

            } else {
                jsonObject.put(me.getKey(), v);
            }
        }
        return jsonObject;

    }


    static boolean isDateObject(Map<String, ?> v) {
        if (v.size() == 1 && v.containsKey("$date")) {
            return true;
        } else {
            return false;
        }
    }

    @SuppressWarnings("unchecked")
    public static JSONArray toJSONArray(List<?> a) {
        JSONArray result = new JSONArray();
        for (Object o : a) {
            if (o instanceof Map) {
                Map<String, ?> map = Map.class.cast(o);

                    result.add(toJSONObject(map));

            } else if (o instanceof List) {
                result.add(toJSONArray((List<?>) o));
            } else {
                result.add(o);
            }
        }
        return result;
    }

    @SuppressWarnings("unchecked")
    public static JSONObject toJSONObject(Map<String, ?> o) {
        JSONObject result = new JSONObject();
        for (Map.Entry<String, ?> me : o.entrySet()) {
            Object v = me.getValue();
            if (v instanceof List) {
                result.put(me.getKey(), toJSONArray((List<?>) v));
            } else if (v instanceof Map) {
                Map map = (Map) v;

                    result.put(me.getKey(), toJSONObject(map));

            } else {
                result.put(me.getKey(), v);
            }
        }
        return result;
    }

    static final class IndexedComponent extends PathComponent {
        private int index;

        private IndexedComponent(int index, String parentPath) {
            super(parentPath);
            this.index = index;
        }

        protected Object walk(Object object) {
            JSONArray jsonArray = asJSONArray(object);
            int length = jsonArray.size();
            if (index < 0 || index >= length) {
                throw new JsonException.Path("no element at index " + index + " (length = " + length + ")");
            }
            return jsonArray.get(index);

        }

        private JSONArray asJSONArray(Object object) {
            if (!(object instanceof JSONArray)) {
                throw new JsonException.Path("expected a ArrayNode under " + getParentPath() + " but found " + object.getClass().getSimpleName() + " instead");
            }
            return (JSONArray) object;
        }

        @Override
        protected String key() {
            return Integer.toString(index);
        }
    }

    static final class NamedComponent extends PathComponent {
        private String name;

        private NamedComponent(String name, String parentPath) {
            super(parentPath);
            this.name = name;
        }

        protected Object walk(Object original) {
            JSONObject jsonObject = asJsonObject(original);
            if (!jsonObject.has(name)) {
                throw new JsonException.Path("no key " + name + " found");
            }
            return jsonObject.get(name);

        }

        private JSONObject asJsonObject(Object original) {
            if (!(original instanceof JSONObject)) {
                throw new JsonException.Path("expected a JSONObject under " + getParentPath() + " but found " + original.getClass().getSimpleName() + " instead");
            }
            return (JSONObject) original;
        }

        @Override
        protected String key() {
            return name;
        }
    }

    abstract static class PathComponent {
        public String getParentPath() {
            return parentPath;
        }

        private final String parentPath;

        protected PathComponent(String parentPath) {
            this.parentPath = parentPath;
        }

        protected abstract Object walk(Object object);

        protected abstract String key();
    }

    /**
     * Creates a JSONObject and reverses keys and values. Shallow. Works only for <String,String>
     * @return
     */
    public JSONObject reverse() {
        JSONObject ret = new JSONObject();
        for(String key : keySet()) {
            ret.put(get(key).toString(), key);
        }
        return ret;
    }
    

}
