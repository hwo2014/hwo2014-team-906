package noobbot.Message;

import com.google.gson.Gson;

/**
 * Created by liu li on 2014-4-15.
 */
abstract public class SendMsg {
    public String toJson() {
        return new Gson().toJson(new ReplayMsgWrapper(this));
    }

    protected Object msgData() {
        return this;
    }



    protected abstract String msgType();
}
