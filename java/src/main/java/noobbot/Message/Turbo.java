package noobbot.Message;


public class Turbo extends SendMsg {
    protected String msgType() {
        return "turbo";
    }

    private String data;
    private Integer gameTicket;

    public Turbo(String data) {
        this.data = data;
    }

    public Turbo(String data, Integer gameTick) {
        this.data = data;
        this.gameTicket = gameTick;
    }

    protected Integer gameTicket() {
        return gameTicket;
    }

    @Override
    protected Object msgData() {
        return data;
    }
}
