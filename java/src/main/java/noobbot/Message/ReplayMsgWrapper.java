package noobbot.Message;

/**
 * Created by liu li on 2014-4-16.
 */
public class ReplayMsgWrapper {

    public final String msgType;
    public final Object data;
    public Integer gameTick;


    ReplayMsgWrapper(final String msgType, final Object data) {
        this.msgType = msgType;
        this.data = data;

    }

    ReplayMsgWrapper(final String msgType, final Object data, final Integer gameTick) {
        this.msgType = msgType;
        this.data = data;
        this.gameTick = gameTick;

    }


    public ReplayMsgWrapper(final SendMsg sendMsg) {
        this(sendMsg.msgType(), sendMsg.msgData());
    }

    public ReplayMsgWrapper(final Throttle throttle) {
        this(throttle.msgType(), throttle.msgData(), throttle.gameTicket());
        System.out.println("throttle");
    }

    public ReplayMsgWrapper(final Switch switchC) {
        this(switchC.msgType(), switchC.msgData(), switchC.gameTicket());
        System.out.println("switch");
    }


}
