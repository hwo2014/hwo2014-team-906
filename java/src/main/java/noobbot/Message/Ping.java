package noobbot.Message;

/**
 * Created by liu li on 2014-4-15.
 */
public class Ping extends SendMsg {
    @Override
    protected String msgType() {
        return "ping";
    }
}
