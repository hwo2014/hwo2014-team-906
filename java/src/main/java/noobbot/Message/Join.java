package noobbot.Message;

/**
 * Created by liu li on 2014-4-15.
 */
public class Join extends SendMsg {
    public  String name;
    public  String key;

    public Join(final String name, final String key) {
        this.name = name;
        this.key = key;
    }

    public String botName() {
         return name;
        }

    public Join() {

    }

    @Override
    protected String msgType() {
        return "join";
    }
}
