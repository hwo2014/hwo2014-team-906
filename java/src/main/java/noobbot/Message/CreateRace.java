package noobbot.Message;

import noobbot.JSON.JSONObject;

public class CreateRace extends Join {

    public String trackName;
    public String password;
    public int carCount;

    public CreateRace(final String name, final String key) {
        this.name = name;
        this.key = key;
    }



    protected Object msgData() {
        JSONObject data = new JSONObject().put("botId", new JSONObject().put("name", name).put("key", key)).put("carCount", carCount);
        if (trackName != null) {
            data.put("trackName", trackName);
        }
        if (password != null) {
            data.put("password", password);
        }
        return data;
    }

    public CreateRace(final String name, final String key, final String trackName, final String password, int carCount) {
        this.name = name;
        this.key = key;
        this.trackName = trackName;
        this.password = password;
        this.carCount = carCount;
    }

    @Override
    protected String msgType() {
        return "createRace";
    }
}
