package noobbot.Message;


public class MsgWrapper {
    public final String msgType;
    public final Object data;
    public final Integer gameTick;

    MsgWrapper(final String msgType, final Object data) {
        this.msgType = msgType;
        this.data = data;
        this.gameTick = 0;
    }

    MsgWrapper(final String msgType, final Object data, final Integer gameTick) {
        this.msgType = msgType;
        this.data = data;
        this.gameTick = gameTick;
    }

    public MsgWrapper(final SendMsg sendMsg) {
        this(sendMsg.msgType(), sendMsg.msgData());
    }

    public MsgWrapper(final SendMsg sendMsg, final Integer gameTick) {
            this(sendMsg.msgType(), sendMsg.msgData(), gameTick);
        }


}
