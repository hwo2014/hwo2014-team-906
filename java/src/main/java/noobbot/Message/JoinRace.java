package noobbot.Message;

import noobbot.JSON.JSONObject;

public class JoinRace extends Join {


    public  String trackName;
    public  String password;
    public int carCount;

    public JoinRace(final String name, final String key, final String trackName, final String password, int carCount)  {
        this.name = name  + Double.valueOf(Math.random() * 10).intValue();
        this.key = key;
        this.trackName = trackName;
        this.password = password;
        this.carCount = carCount;
    }

    protected Object msgData() {
            JSONObject data = new JSONObject().put("botId", new JSONObject().put("name", name).put("key", key));
            if (trackName != null) {
                data.put("trackName", trackName);
            }
            if (password != null) {
                data.put("password", password);
            }
        data.put("carCount", carCount);
            return data;
        }

    @Override
    protected String msgType() {
        return "joinRace";
    }
}
