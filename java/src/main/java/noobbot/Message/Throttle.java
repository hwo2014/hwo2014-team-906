package noobbot.Message;

/**
 * Created by liu li on 2014-4-15.
 */
public class Throttle extends SendMsg {
    private double value;
    private Integer gameTicket;

    public Throttle(double value) {
        this.value = value;
    }

    public Throttle(double value, Integer gameTick) {
        this.value = value;
        this.gameTicket = gameTick;
    }

    @Override
    protected Object msgData() {
        return value;
    }

    protected Integer gameTicket() {
           return gameTicket;
       }

    @Override
    protected String msgType() {
        return "throttle";
    }


}
