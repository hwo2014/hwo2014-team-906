package noobbot.Message;

/**
 * Created by liu li on 2014-4-18.
 */
public class Switch extends SendMsg {
   private String data;
    private Integer gameTicket;


    @Override
    protected String msgType() {
        return "switchLane";
    }

    public Switch(String data) {
       this.data = data;
    }

    public Switch(String data, Integer gameTick) {
            this.data = data;
            this.gameTicket = gameTick;
        }

    protected Integer gameTicket() {
           return gameTicket;
       }

    @Override
       protected Object msgData() {
           return data;
       }
}
