package noobbot;

import com.google.gson.Gson;
import noobbot.Controller.Controller;
import noobbot.JSON.JSONObject;
import noobbot.Message.*;

import java.io.*;
import java.net.Socket;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.LinkedBlockingQueue;

public class Main {
    public static void main(String... args) throws IOException {
        String host = args[0];
        int port = Integer.parseInt(args[1]);
        String botName = args[2];
        String botKey = args[3];
        String trackName = null;
        String password = null;
        boolean isCreateRace = false;
        int carCount = 0;
        if (args.length >= 5) {
            isCreateRace = Boolean.parseBoolean(args[4]);
        }
        if (args.length >= 6) {
            carCount = Integer.parseInt(args[5]);
        }
        if (args.length >= 7) {
            trackName = args[6];
        }
        if (args.length >= 8) {
            password = args[7];
        }

        System.out.println("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);

        final Socket socket = new Socket(host, port);
        final PrintWriter writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), "utf-8"));

        final BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), "utf-8"));
        Join msg;
        if (trackName == null || trackName.equals("")) {
            msg = new Join(botName, botKey);
            botName = msg.botName();
            new Main(reader, writer, msg, botName);
        } else if (!isCreateRace) {
            msg = new JoinRace(botName, botKey, trackName, password, carCount);
            botName = msg.botName();

        } else {
            msg = new CreateRace(botName, botKey, trackName, password, carCount);
            botName = msg.botName();
            new Main(reader, writer, msg, botName);
        }

        new Main(reader, writer, msg, botName);
    }

    final Gson gson = new Gson();
    private PrintWriter writer;

    public Main(final BufferedReader reader, final PrintWriter writer, final Join join, String botName) throws IOException {
        BlockingQueue<MsgWrapper> inQueue = new LinkedBlockingQueue<>();
        ConcurrentLinkedQueue<SendMsg> outQueue = new ConcurrentLinkedQueue<>();
        Controller controller = new Controller(botName);
        //startConsumer(inQueue, outQueue, controller, writer);
        this.writer = writer;
        String line = null;
        printInfo("Sending join/createRace " + join.toJson());
        send(join);

        while ((line = reader.readLine()) != null) {
            final MsgWrapper msgFromServer = gson.fromJson(line, MsgWrapper.class);
            inQueue.add(msgFromServer);
            JSONObject sendMsg = controller.compute(msgFromServer);
            if (sendMsg != null) {
                send(sendMsg);
            }
        }
    }



    private void send(final JSONObject msg) {
        printInfo("Send" + msg.toString());
        printInfo("");
        writer.println(msg.toString());
        writer.flush();
    }

    private void send(final SendMsg msg) {
        writer.println(msg.toJson());
        writer.flush();
    }

    private void printInfo(String message) {
           System.out.println(message);
       }
}

