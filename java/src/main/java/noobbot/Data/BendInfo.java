package noobbot.Data;


import noobbot.Controller.Controller;
import noobbot.JSON.JSONObject;

import java.util.ArrayList;
import java.util.List;

public final class BendInfo {
    public List<JSONObject> bends = new ArrayList<>();
    public double bendLengthTotal = 0;
    public List<Double> laneBendLength = new ArrayList<>(); //bendLength in each lane
    public List<Double> laneCurvatureLength = new ArrayList<>(); //bendCurvature in each lane
    public List<Double> laneTimeDuration = new ArrayList<>();
    public List<Double> maximumEnteringSpeeds = new ArrayList<>();
    public List<Double> coherenceSpeeds = new ArrayList<>();
    public double radius;
    public double angle;
    public double totalAngle;
    public int pieceNumber = 0;

    public void updateMaximumEnteringSpeeds(int lane) {
        double coherence = coherenceSpeeds.get(lane);
        double bendRadius = radius;
        if (angle > 0) {
            bendRadius -= ((JSONObject) Controller.lanes.get(lane)).getAsDouble("distanceFromCenter");
        } else {
            bendRadius += ((JSONObject) Controller.lanes.get(lane)).getAsDouble("distanceFromCenter");
        }
        double speed = Math.sqrt(bendRadius * coherence);
        maximumEnteringSpeeds.set(lane, Double.valueOf(speed));
    }

}
