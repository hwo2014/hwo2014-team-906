package noobbot.Data;


public class NextTurnInfo {
    public double distanceToNextPieceInDifferentCurvature = 0;
    public int pieceIndexOfNextDifferentCurvature = 0;

    public double distanceToNextPieceInDifferentDirection = 0;
        public int pieceIndexOfNextDifferentDirection = 0;

    public void setNextTurnInfo(double distance, int pieceIndex) {
        this.distanceToNextPieceInDifferentCurvature = distance;
        this.pieceIndexOfNextDifferentCurvature = pieceIndex;
    }


    public void setNextTurnInfoDifferentDirection(double distance, int pieceIndex) {
           this.distanceToNextPieceInDifferentDirection = distance;
           this.pieceIndexOfNextDifferentDirection = pieceIndex;
       }
}
