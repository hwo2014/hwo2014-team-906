package noobbot.Controller;


import noobbot.JSON.JSONObject;
import noobbot.Message.MsgWrapper;
import noobbot.Message.SendMsg;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.PrintWriter;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentLinkedQueue;

public class Consumer implements Runnable {
    BlockingQueue<MsgWrapper> inQueue;
    private static final Logger logger = LoggerFactory.getLogger(
            Consumer.class.getName());

    private ConcurrentLinkedQueue<SendMsg> outQueue;
    private Controller controller;
    private PrintWriter writer;

    public Consumer(BlockingQueue<MsgWrapper> inQueue, ConcurrentLinkedQueue<SendMsg> outQueue, Controller controller, PrintWriter writer) {
        this.inQueue = inQueue;
        this.outQueue = outQueue;
        this.controller = controller;
        this.writer = writer;
    }

    private void send(final JSONObject msg) {
        System.out.println("Send" + msg.toString());
           writer.println(msg.toString() );
           writer.flush();
       }

    @Override
    public void run() {
        logger.info("Consumer start ");
        while (!Controller.stopConsumer) {
            try {
                MsgWrapper temp = inQueue.take();
                JSONObject sendMsg = controller.compute(temp);
                if (sendMsg != null) {
                    send(sendMsg);
                }

            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}

