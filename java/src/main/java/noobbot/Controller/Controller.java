package noobbot.Controller;

//M6xkWVL9Eb

import noobbot.Data.BendEnteringInfo;
import noobbot.Data.BendInfo;
import noobbot.Data.NextTurnInfo;
import noobbot.JSON.JSONArray;
import noobbot.JSON.JSONObject;
import noobbot.Message.MsgWrapper;

import java.util.*;

public class Controller {

    public static double FRACTION = 0.17;//0.0074201;
    public static double coherenceForSpeed = 0.515d;// 0.468;   //kemola 0.65
    public static double coherenceDecreaseFactor = 0.05d;
    public static double coherenceInitial = coherenceForSpeed;
    public static double accelerationG = 0;
    public static double FRACTION_TURBO = FRACTION;
    double maxBendSpeed = 0;
    double maxBendAngle = 0;
    double oldCoherenceInBend = 0;
    boolean isCoherenceChange = false;
    boolean hasCoherenceChangedByAngle = false;


    String botName;

    public static boolean stopConsumer = false;
    TreeMap<Integer, JSONObject> switches = new TreeMap<>(); //integer, index of piece
    TreeMap<Integer, JSONObject> bends = new TreeMap<>(); //integer, index of piece
    int havingCrashPieceIndex = -1;
    int havingCrashLaneIndex = -1;
    String trackName = "";

    boolean isTurboing = false;
    List<Double> fractionList = new ArrayList();
    BendEnteringInfo bendEnteringInfo;


    JSONArray pieces = new JSONArray();
    TreeMap<Integer, BendInfo> switchBendsMap = new TreeMap<>();  //integer, index of piece which has the switch
    int numberOfLane = 0;
    public static JSONArray lanes = new JSONArray();
    TreeMap<Integer, Integer> shortestPath = new TreeMap<>(); //key integer, index of pieces which has the switch, value integer, the laneIndex which is in the shortest path
    TreeMap<Integer, Integer> minimumCurvaturePath = new TreeMap<>(); //key integer, index of pieces which has the switch, value integer, the laneIndex which is in the shortest path
    TreeMap<Integer, Integer> shortestTimePath = new TreeMap<>(); //key integer, index of pieces which has the switch, value integer, the laneIndex which is in the shortest path
    TreeMap<Integer, BendInfo> bendEnterMap = new TreeMap<>(); // key integer index of pieces of bends


    static int switchCurrentPieceIndex = -1;   //The index of piece which has the switch
    TreeMap<Integer, Integer> carTravelRoute = new TreeMap<>();    // //key integer, index of pieces which has the switch, value integer, the laneIndex

    String myCarColor = "";
    JSONObject myCar;
    JSONArray carsPosition = new JSONArray();
    Integer lapCount = 0;
    Map<Integer, Double> lapLength = new HashMap<>();

    Map<Integer, Map<Integer, Double>> pieceLaneLengthMap = new HashMap<>();  //integer: index of piece, integer: laneIndex, double: length of lane
    Map<Integer, Integer> nextDifferentCurvatureBendIndexMapForPiece = new HashMap<>(); //integer: index of straight piece,  integer: index of bend piece
    Map<Integer, Map<Integer, Double>> bendLaneRadiusMap = new HashMap<>();  //integer: index of piece, integer: laneIndex, double: radius of lane
    Map<Integer, JSONObject> curvatureChangeBends = new HashMap<>();  //integer, index of the end bend to which next bend has different curvature  JSONObject: bend in different curvature


    boolean isTurboAvailable = false;
    Double minimumBendSpeedAll = 10000d;
    Double lastAngleDiff = 0d;
    int overTakenSwitchIndex = -1;
    String overTakenDirection = "Left";
    double lastThrottle = 0;
    double lastCarAngle = 0;
    boolean useTurBoToHit = false;
    JSONObject lastCarInFront;
    boolean isCrash = false;
    int crashCount = 0;
    static int coherenceDecreaseThreshHold = 1;
    static int lastCoherenceDecreaseThreshHold = 1;


    private void resetCoherenceRelated() {
        // coherenceForSpeed = 0.515d;
        coherenceDecreaseThreshHold = 1;
        lastCoherenceDecreaseThreshHold = 1;
        crashCount = 0;
    }


    private double getCoherence(double radius) {
        if (radius <= 110) {
            return coherenceForSpeed * Math.sqrt(radius / 90) * 100;
        }
        return coherenceForSpeed;
    }

    private double getCoherenceForSpeed(double radius) {

        if (radius >= 180) {
            return coherenceForSpeed + 0.05;
        }

        if (trackName.equals("pentag")) {
            return 0.5;
        }

        return coherenceForSpeed;
    }

    private double getCoherenceForSpeed(int index, int lane) {
        double coherence = bendEnterMap.get(bendEnterMap.floorKey(index)).coherenceSpeeds.get(lane);
        if (coherence < 0) {
            coherence = 0.3;
        }
        return coherence;
    }

    private void decreaseCoherence(int index, int laneStart, int laneEnd) {
        if (!trackName.equals("pentag")) {
            for (int i = 0; i < lanes.size(); i++) {
                double coherence = bendEnterMap.get(bendEnterMap.floorKey(index)).coherenceSpeeds.get(i);
                coherence -= coherenceDecreaseFactor;
                if (coherence < 0) {
                    coherence = 0.3d;
                }
                bendEnterMap.get(bendEnterMap.floorKey(index)).coherenceSpeeds.set(i, coherence);
                try {
                    Map.Entry<Integer, BendInfo> lowerEntry = bendEnterMap.lowerEntry(bendEnterMap.floorKey(index));
                    if (lowerEntry != null && lowerEntry.getValue().pieceNumber + lowerEntry.getKey() == bendEnterMap.floorKey(index)) {
                        bendEnterMap.get(lowerEntry.getKey()).coherenceSpeeds.set(i, coherence);
                    }
                } catch (Exception e) {

                }
            }
        } else {
            for (Map.Entry<Integer, BendInfo> entry : bendEnterMap.entrySet()) {
                for (int i = 0; i < lanes.size(); i++) {
                    double coherence = entry.getValue().coherenceSpeeds.get(i);
                    coherence -= coherenceDecreaseFactor;
                    if (coherence < 0) {
                        coherence = 0.3d;
                    }
                    entry.getValue().coherenceSpeeds.set(i, coherence);
                }
            }
        }
        printInfo("decrease " + bendEnterMap.floorKey(index) + " coherenceForSpeed by " + coherenceDecreaseFactor);

    }

    private Double calculateCurrentSpeed(double distanceInPiece, double lastTicketDistanceToStartPiece) {
        return (distanceInPiece - lastTicketDistanceToStartPiece);
    }

    private int getNextBendIndex(int currentPieceIndex) {
        if (currentPieceIndex > bends.lastKey()) {  //straight after last bend
            return bends.firstKey();
        } else {
            return bends.ceilingKey(currentPieceIndex);
        }
    }


    private NextTurnInfo calculateDistanceToNextBend(int currentPieceIndex, double distanceInPiece, int currentLaneStartIndex, int currentLaneEndIndex, boolean isStraight) {
        double distanceToTheBend = 0;
        double currentLaneLength = getLaneLength(currentPieceIndex, currentLaneStartIndex, currentLaneEndIndex);
        NextTurnInfo nextTurnInfo = new NextTurnInfo();

        if (currentPieceIndex > bends.lastKey()) {  //straight after last bend
            distanceToTheBend = currentLaneLength - distanceInPiece;
            for (int i = currentPieceIndex + 1; i < pieces.size(); i++) {
                distanceToTheBend += getLaneLength(i, currentLaneStartIndex, currentLaneEndIndex);
                nextTurnInfo.setNextTurnInfo(distanceToTheBend, bends.firstKey());
            }
            for (int i = 0; i < bends.firstKey(); i++) {
                distanceToTheBend += getLaneLength(i, currentLaneStartIndex, currentLaneEndIndex);
                nextTurnInfo.setNextTurnInfo(distanceToTheBend, bends.firstKey());
            }

        } else {
            for (int i = 1; i < bends.ceilingKey(currentPieceIndex) + 1; i++) {
                if (bends.get(i + currentPieceIndex) == null) {
                    distanceToTheBend += getLaneLength(i + currentPieceIndex, currentLaneStartIndex, currentLaneEndIndex);
                } else {
                    distanceToTheBend += currentLaneLength - distanceInPiece;
                    nextTurnInfo.setNextTurnInfo(distanceToTheBend, i + currentPieceIndex);
                    break;
                }
            }
        }
        return nextTurnInfo;
    }

    private NextTurnInfo calculateDistanceToNextPieceInDifferentCurve(int currentPieceIndex, double distanceInPiece, int currentLaneStartIndex, int currentLaneEndIndex) {     //for bend
        NextTurnInfo nextTurnInfo = new NextTurnInfo();
        double distanceToNextDifferentCurvature = 0;
        double distanceToNextDifferentDirection = 0;
        double currentLaneLength = getLaneLength(currentPieceIndex, currentLaneStartIndex, currentLaneEndIndex);
        JSONObject bend = bends.get(currentPieceIndex);
        Double angle = bend.getAsDouble("angle");
        Double radius = bend.getAsDouble("radius");
        int startIndex1 = 0;
        int startIndex2 = 0;
        int nextDifferentCIndex = currentPieceIndex + 1 < pieces.size() ? currentPieceIndex + 1 : currentPieceIndex + 1 - pieces.size();
        int nextDifferentDIndex = nextDifferentCIndex;
        if (!(currentPieceIndex == pieces.size() - 1) || !(bends.firstKey() == 0)) { //not last piece or the first piece is not 0
            startIndex1 = 1;
            startIndex2 = currentPieceIndex;
        }
        int i;
        int bendIndex;
        boolean diffCurvatureAlreadyFound = false;
        for (i = startIndex1; i < pieces.size(); i++) {
            if (startIndex2 + i < pieces.size()) {
                bendIndex = startIndex2 + i;
            } else {
                bendIndex = startIndex2 + i - pieces.size();
            }
            JSONObject nextBend = bends.get(bendIndex);
            if (nextBend != null && nextBend.getAsDouble("angle") * angle > 0) {
                double laneLength = getLaneLength(bendIndex, currentLaneStartIndex, currentLaneEndIndex);
                if (nextBend.getAsDouble("radius").intValue() == radius.intValue()) {
                    if (!diffCurvatureAlreadyFound) {
                        distanceToNextDifferentCurvature += laneLength;
                        nextDifferentCIndex = bendIndex + 1 < pieces.size() ? bendIndex + 1 : bendIndex + 1 - pieces.size();
                    }
                } else {
                    diffCurvatureAlreadyFound = true;
                }
                distanceToNextDifferentDirection += laneLength;
            } else {
                nextDifferentDIndex = bendIndex;
                break;
            }
        }

        distanceToNextDifferentCurvature += currentLaneLength - distanceInPiece;
        distanceToNextDifferentDirection += currentLaneLength - distanceInPiece;
        nextTurnInfo.setNextTurnInfo(distanceToNextDifferentCurvature, nextDifferentCIndex);
        nextTurnInfo.setNextTurnInfoDifferentDirection(distanceToNextDifferentDirection, nextDifferentDIndex);
        return nextTurnInfo;
    }

    private BendEnteringInfo throttleZero(double currentSpeed, double distanceToTheBend, int currentPieceIndex, int currentLaneStartIndex, int currentLaneEndIndex, double radius, int bendIndex, double angle, double currentAngle) {
        BendEnteringInfo enteringInfo = new BendEnteringInfo();
        Double maxbes = maxiMumBendEnteringSpeed(currentPieceIndex, currentLaneStartIndex, currentLaneEndIndex, radius, currentSpeed, bendIndex, 1, angle, currentAngle);
        printInfo("Maxmum bend entering speed " + maxbes);
        if (currentSpeed < maxbes) {
            if (maxbes - currentSpeed < 0.02) {
                enteringInfo.shouldBreak = true;
            } else {
                enteringInfo.shouldBreak = false;
            }
            enteringInfo.maxBendEnteringSpeed = maxbes;
            return enteringInfo;
        }
        printInfo("Distance to next bend  " + distanceToTheBend);
        double diff;
        if (!isTurboing) {
            diff = (currentSpeed * currentSpeed - maxbes * maxbes) / (2 * FRACTION) - distanceToTheBend;
        } else {
            diff = (currentSpeed * currentSpeed - maxbes * maxbes) / (2 * FRACTION_TURBO) - distanceToTheBend;
        }
        //printInfo(currentSpeed + " " + maxbes + " " + FRACTION + " " + distanceToTheBend);
        printInfo("Max brake distance compared to next bend " + diff);
        enteringInfo.shouldBreak = diff > -50;
        enteringInfo.maxBendEnteringSpeed = maxbes;
        return enteringInfo;
    }


    private Double getNextTickRunningDistance(Double currentSpeed, Double accelerate) {
        if (lastThrottle > 0.5 && accelerate > 0) {
            return currentSpeed + Math.abs(accelerate);
        }

        if (lastThrottle > 0.5) {
            return currentSpeed + Math.abs(accelerate);
        }
        return currentSpeed + accelerate;
    }

    public Controller(String botName) {
        this.botName = botName;
    }

    private void initLaneLengthInEachPiece() {
        for (int i = 0; i < pieces.size(); i++) {
            Map<Integer, Double> laneLengthMap = new HashMap<>();
            if (switches.get(i) == null) {
                for (int j = 0; j < lanes.size(); j++) {
                    double laneLength = getLaneLength(i, j, j);
                    laneLengthMap.put(j, laneLength);
                }
                pieceLaneLengthMap.put(i, laneLengthMap);
            }
        }
    }

    private Double getLaneLength(int pieceIndex, int laneStartIndex, Integer laneEndIndex) {
        Map<Integer, Double> laneLengthMap = pieceLaneLengthMap.get(pieceIndex);
        if (laneLengthMap != null && (laneEndIndex == null || laneEndIndex == laneStartIndex)) {
            Double value = laneLengthMap.get(laneStartIndex);
            if (value != null) {
                return value;
            }
        }

        JSONObject piece = (JSONObject) pieces.get(pieceIndex);
        if (piece.contains("length")) {
            if (laneEndIndex == null || laneEndIndex == laneStartIndex) {
                return piece.getAsDouble("length");
            } else {
                double laneWidth = Math.abs(((JSONObject) lanes.get(laneEndIndex)).getAsDouble("distanceFromCenter") - ((JSONObject) lanes.get(laneStartIndex)).getAsDouble("distanceFromCenter"));
                return Math.sqrt(piece.getAsDouble("length") * piece.getAsDouble("length") + laneWidth * laneWidth);
            }
        }

        double radius = piece.getAsDouble("radius");
        double angle = piece.getAsDouble("angle");
        double arcLength = Math.abs(angle) * radius * Math.PI / 180;


        if (laneEndIndex == null || laneEndIndex == laneStartIndex) {
            return getArcLengthByLane(laneStartIndex, angle, radius, arcLength);
        } else {
            return Math.max(getArcLengthByLane(laneStartIndex, angle, radius, arcLength), getArcLengthByLane(laneEndIndex, angle, radius, arcLength)) * 0.97;
        }
    }

    private double getArcLengthByLane(int laneIndex, double angle, double radius, double arcLengthCenter) {
        JSONObject lane = (JSONObject) lanes.get(laneIndex);
        double distanceFromCenter = lane.getAsDouble("distanceFromCenter");
        double ratio;
        if (angle > 0) {
            ratio = (radius - distanceFromCenter) / radius;
        } else {
            ratio = (radius + distanceFromCenter) / radius;
        }

        return arcLengthCenter * ratio;
    }

    public JSONObject compute(final MsgWrapper msgFromServer) {
        if (msgFromServer.msgType.equals("carPositions")) {
            try {
                return generateAction(msgFromServer);
            } catch (Exception e) {
                e.printStackTrace();
                return new JSONObject().put("msgType", "throttle").put("data", 0.4d);
            }
        } else if (msgFromServer.msgType.equals("join")) {
            printInfo("Joined");
        } else if (msgFromServer.msgType.equals("yourCar")) {
            JSONObject data = new JSONObject().put("data", JSONObject.toJSONObject(new JSONObject(), (Map) msgFromServer.data));
            myCarColor = data.getObject("data.color", "").toString();
            return new JSONObject().put("msgType", "ping");
        } else if (msgFromServer.msgType.equals("gameInit")) {
            printInfo("Race init");
            initGame(msgFromServer);
            return new JSONObject().put("msgType", "ping");
        } else if (msgFromServer.msgType.equals("gameEnd")) {
            //printLaneLengthTotal();
            printInfo("Minum bend speed " + minimumBendSpeedAll);
            printInfo("Race end");
            resetCoherenceRelated();
        } else if (msgFromServer.msgType.equals("gameStart")) {
            printInfo("Race start");
            resetCoherenceRelated();
            return new JSONObject().put("msgType", "ping");
        } else if (msgFromServer.msgType.equals("turboAvailable")) {
            printInfo(msgFromServer.msgType.toString());
            isTurboAvailable = true;
            printInfo(msgFromServer.data.toString() + " game tick " + msgFromServer.gameTick);
            return new JSONObject().put("msgType", "ping");
        } else if (msgFromServer.msgType.equals("lapFinished")) {
            printInfo(msgFromServer.msgType.toString());
            lapCount++;
            //lapLength.put(lapCount, printTravelRoute());
            carTravelRoute.clear();
            return new JSONObject().put("msgType", "ping");
        } else if (msgFromServer.msgType.equals("spawn")) {
            printInfo(msgFromServer.msgType.toString());
            String spawnColor = getColorFromMessageData(msgFromServer);
            if (spawnColor.equals(myCarColor)) {
                overTakenSwitchIndex = -1;
                isCrash = false;
            }
            return new JSONObject().put("msgType", "ping");
        } else if (msgFromServer.msgType.equals("crash")) {
            printInfo(msgFromServer.msgType.toString());
            String crashColor = getColorFromMessageData(msgFromServer);
            Integer pieceIndex = ((Double) myCar.getObject("piecePosition.pieceIndex", 0d)).intValue();
            Integer currentLaneStartIndex = ((Double) myCar.getObject("piecePosition.lane.startLaneIndex", 0d)).intValue();
            Integer currentLaneEndIndex = ((Double) myCar.getObject("piecePosition.lane.endLaneIndex", 0d)).intValue();
            if (crashColor.equals(myCarColor)) {
                crashCount++;
                isCrash = true;
                decreaseCoherence(pieceIndex, currentLaneStartIndex, currentLaneEndIndex);
            }
            return new JSONObject().put("msgType", "ping");
        } else if (msgFromServer.msgType.equals("turboStart")) {
            String turboingColor = getColorFromMessageData(msgFromServer);
            printInfo(msgFromServer.msgType.toString());
            isTurboing = (turboingColor.equals(myCarColor));
            return new JSONObject().put("msgType", "ping");
        } else if (msgFromServer.msgType.equals("turboEnd")) {
            printInfo(msgFromServer.msgType.toString());
            String turboingColor = getColorFromMessageData(msgFromServer);
            isTurboing = isTurboing && !(turboingColor.equals(myCarColor));
            return new JSONObject().put("msgType", "ping");
        } else {
            printInfo(msgFromServer.msgType.toString());
            return new JSONObject().put("msgType", "ping");
        }

        return null;
    }

    private String getColorFromMessageData(final MsgWrapper msgFromServer) {
        JSONObject data = JSONObject.toJSONObject(new JSONObject(), (Map) msgFromServer.data);
        return data.getObject("color", "").toString();
    }

    private void initCarsInfo(JSONArray currentCarPosition) {
        boolean isFirstTime = false;
        if (carsPosition.size() == 0) {
            isFirstTime = true;
            carsPosition = new JSONArray(currentCarPosition);
        }
        for (Object obj : carsPosition) {
            JSONObject car = (JSONObject) obj;
            car.put("lastTickSpeed", car.getObject("currentSpeed", Double.valueOf(0d)));
            JSONObject carInfoInTime = currentCarPosition.findFirstBy("id.color", car.getObject("id.color", ""));
            if (carInfoInTime != null) {
                Double angle = ((Double) carInfoInTime.getObject("angle", 0d));
                JSONObject piecePosition = ((JSONObject) carInfoInTime.getObject("piecePosition", new JSONObject()));
                Integer pieceIndex = ((Double) carInfoInTime.getObject("piecePosition.pieceIndex", 0d)).intValue();
                Integer currentLaneStartIndex = ((Double) carInfoInTime.getObject("piecePosition.lane.startLaneIndex", 0d)).intValue();
                Integer currentLaneEndIndex = ((Double) carInfoInTime.getObject("piecePosition.lane.endLaneIndex", 0d)).intValue();
                Double distanceInPiece = ((Double) carInfoInTime.getObject("piecePosition.inPieceDistance", 0d));
                Double currentPieceLaneLength = getLaneLength(pieceIndex.intValue(), currentLaneStartIndex.intValue(), currentLaneEndIndex.intValue()); //todo get exactly laneIndex
                double speed = isFirstTime ? 0 : getCurrentSpeed(car, carInfoInTime);
                if (speed - (Double) car.getObject("lastTickSpeed", 0) >= 2.8) {   //todo crash
                    speed = (Double) car.getObject("lastTickSpeed", Double.valueOf(0d));
                }
                double acceleration = speed - (Double) car.getObject("lastTickSpeed", 0);
              /*  if (speed > (Double) car.getObject("lastTickSpeed", 0)) {
                    if (accelerationList.size() < 5) {
                        accelerationList.add(acceleration);
                    } else {
                        accelerationList.remove(0);
                        accelerationList.add(acceleration);
                    }
                }
                accelerationG = calculateAverageList(accelerationList);
                printInfo("acceleration is " + accelerationG); */
                car.put("currentSpeed", speed);
                car.put("totalSpeed", (Double) car.getObject("totalSpeed", 0d) + speed);
                car.put("currentPieceLaneLength", currentPieceLaneLength);
                car.put("lastPieceLaneLength", currentPieceLaneLength);
                if (!car.getObject("id.color", "").equals(myCarColor)) {
                    car.put("lastPieceIndex", pieceIndex);
                }
                car.put("lastTicketDistanceInPiece", distanceInPiece);
                car.put("angle", angle);
                car.put("piecePosition", piecePosition);
                car.put("currentLaneEndIndex", currentLaneEndIndex);
                car.put("currentLaneStartIndex", currentLaneStartIndex);
                car.put("currentAccelerate", acceleration);
                printInfo(car.toString());
                calculateFraction(lastThrottle, speed, (Double) car.getObject("lastTickSpeed", 0));

            }
        }

    }

    private double calculateAverageList(List<Double> list) {
        double aveF = 0;
        for (Double data : list) {
            aveF += data;
        }
        return list.size() > 0 ? aveF / list.size() : 0;
    }

    private void calculateFraction(double lastThrottle, double currentSpeed, double lastTickSpeed) {
        if (lastThrottle == 0 && currentSpeed != 0) {
            double possible = Math.abs(currentSpeed - lastTickSpeed);
            if (possible > 1) {
                possible = FRACTION;
            }

            if (!isTurboing) {
                if (fractionList.size() < 10) {
                    fractionList.add(possible);
                } else {
                    fractionList.remove(0);
                    fractionList.add(possible);
                }
                double aveF = 0;
                for (Double fraction : fractionList) {
                    aveF += fraction;
                }
                FRACTION = aveF / fractionList.size();
                FRACTION_TURBO = FRACTION + 0.02;
            } else {
                //  FRACTION_TURBO = (possible + FRACTION_TURBO) / 2;
            }
        }

        printInfo("fraction is " + FRACTION + " Fraction turbo is " + FRACTION_TURBO);
    }


    private Double getCurrentSpeed(JSONObject car, JSONObject carInTime) {
        double lastTicketDistanceToStartPiece = car.get("lastTicketDistanceInPiece") == null ? 0 : car.getAsDouble("lastTicketDistanceInPiece");
        Integer lastPieceIndex = car.get("lastPieceIndex") == null ? 0 : (Integer) car.get("lastPieceIndex");
        Integer pieceIndex = ((Double) carInTime.getObject("piecePosition.pieceIndex")).intValue();
        if (lastPieceIndex != pieceIndex) {
            Double lastTicketDistanceInPiece = car.get("lastTicketDistanceInPiece") == null ? 0 : car.getAsDouble("lastTicketDistanceInPiece");
            Double lastPieceLaneLength = car.get("lastPieceLaneLength") == null ? 0 : car.getAsDouble("lastPieceLaneLength");
            lastTicketDistanceToStartPiece = lastTicketDistanceInPiece - lastPieceLaneLength;
            //printInfo("Last piece length " + lastPieceLaneLength);
        }
        return calculateCurrentSpeed((Double) carInTime.getObject("piecePosition.inPieceDistance"), lastTicketDistanceToStartPiece);
    }

    private Double getCurrentLaneRadius(int currentPieceIndex, int currentLaneIndex) {
        Double radius = null;
        Map<Integer, Double> laneRadiusMap = bendLaneRadiusMap.get(currentPieceIndex);
        if (laneRadiusMap != null) {
            radius = laneRadiusMap.get(currentLaneIndex);
        }
        if (radius == null) {
            radius = bends.get(currentPieceIndex).getAsDouble("radius");
            Double angleInBend = bends.get(currentPieceIndex).getAsDouble("angle");
            if (angleInBend > 0) {
                radius -= ((JSONObject) lanes.get(currentLaneIndex)).getAsDouble("distanceFromCenter");
            } else {
                radius += ((JSONObject) lanes.get(currentLaneIndex)).getAsDouble("distanceFromCenter");
            }
            laneRadiusMap = bendLaneRadiusMap.get(currentPieceIndex);
            if (laneRadiusMap == null) {
                laneRadiusMap = new HashMap<>();
            }
            laneRadiusMap.put(currentLaneIndex, radius);
            bendLaneRadiusMap.put(currentPieceIndex, laneRadiusMap);
        }
        return radius;
    }

    private int getOverTakenTargetLaneIndex(int currentLaneIndex, String overTakenDirection) {
        if (overTakenDirection.equals("Right")) {
            return currentLaneIndex + 1;
        } else {
            return currentLaneIndex - 1;
        }
    }

    private JSONObject switchLane(JSONObject switchLane, int pieceIndex, boolean isCloseToLaneEnd, int currentLaneStartIndex, int currentLaneEndIndex, Integer gameTick) {
        if (switchLane != null) {
            if (switchCurrentPieceIndex != pieceIndex) {
                switchCurrentPieceIndex = isCloseToLaneEnd ? pieceIndex + 1 : pieceIndex;
                printInfo("About to handle switch in " + switchCurrentPieceIndex + "th piece");
                if (overTakenSwitchIndex != -1) {
                    JSONObject slowestCarInTargetLane = getSlowestCarInFront(pieceIndex, getOverTakenTargetLaneIndex(currentLaneEndIndex, overTakenDirection));
                    JSONObject slowestCarInCurrentLane = getSlowestCarInFront(pieceIndex, currentLaneEndIndex);
                    if (slowestCarInTargetLane != null && slowestCarInCurrentLane != null) {
                        if (isFaster(slowestCarInCurrentLane, slowestCarInTargetLane) && isFaster(carsPosition.findFirstBy("id.color", myCarColor), slowestCarInTargetLane)) {
                            printInfo("There is car in the target switch lane and slower than current lane");
                            overTakenSwitchIndex = -1;
                            return null;
                        }
                    }

                    if (slowestCarInTargetLane != null && slowestCarInCurrentLane == null && isFaster(carsPosition.findFirstBy("id.color", myCarColor), slowestCarInTargetLane)) {
                        printInfo("There is car in the target switch lane slower tham me");
                        overTakenSwitchIndex = -1;
                        return null;
                    }

                    printInfo("Overtaken switch is " + overTakenSwitchIndex);
                    overTakenSwitchIndex = -1;
                    havingCrashLaneIndex = -1;
                    return gameTick == null ? new JSONObject().put("msgType", "switchLane").put("data", overTakenDirection) : new JSONObject().put("msgType", "switchLane").put("data", overTakenDirection).put("gameTick", gameTick);

                }


                Integer targetLaneIndexShortestPath = shortestPath.get(switchCurrentPieceIndex);
                //Integer targetLaneIndexMinimumCurvaturePath = minimumCurvaturePath.get(switchCurrentPieceIndex);
                Integer targetLaneIndex = targetLaneIndexShortestPath;
                if (targetLaneIndex != null) {
                    printInfo("Current lane " + currentLaneStartIndex);
                    printInfo("Target lane " + targetLaneIndex);
                    if (isLaneHavingCarSlowerThanMe(pieceIndex, targetLaneIndex)) { //Todo check the speed  of car in current lane
                        printInfo("Target lane has slower car.");
                        return null;
                    }
                    if (currentLaneStartIndex != targetLaneIndex) {
                        havingCrashLaneIndex = -1;
                        if (currentLaneStartIndex < targetLaneIndex) {
                            return gameTick == null ? new JSONObject().put("msgType", "switchLane").put("data", "Right") : new JSONObject().put("msgType", "switchLane").put("data", "Right").put("gameTick", gameTick);
                        } else {
                            return gameTick == null ? new JSONObject().put("msgType", "switchLane").put("data", "Left") : new JSONObject().put("msgType", "switchLane").put("data", "Left").put("gameTick", gameTick);
                        }
                    }
                }
            }
        }
        return null;
    }


    public JSONObject generateAction(MsgWrapper msgFromServer) {
        // printInfo("Receive " + msgFromServer.data.toString() + " " + msgFromServer.gameTick);
        Integer gameTick = msgFromServer.gameTick;
        JSONArray dataInTime = new JSONArray((List) msgFromServer.data);
        // JSONObject data = new JSONObject("{\"data\":" + msgFromServer.data.toString() + "}");
        // JSONArray dataInTime = data.getJSONArray("data");
        initCarsInfo(dataInTime);
        myCar = carsPosition.findFirstBy("id.color", myCarColor);
        Integer pieceIndex = ((Double) (myCar.getObject("piecePosition.pieceIndex"))).intValue();
        Integer currentLaneStartIndex = ((Double) (myCar.getObject("piecePosition.lane.startLaneIndex"))).intValue(); //todo get exactly laneIndex
        Integer currentLaneEndIndex = ((Double) (myCar.getObject("piecePosition.lane.endLaneIndex"))).intValue(); //todo get exactly laneIndex
        Double distanceInPiece = (Double) (myCar.getObject("piecePosition.inPieceDistance"));
        Double angle = (Double) (myCar.getObject("angle"));
        Double radius = 0d;
        JSONObject switchLane;
        boolean isStraight = bends.get(pieceIndex.intValue()) == null;
        Double currentPieceLaneLength = getLaneLength(pieceIndex.intValue(), currentLaneStartIndex.intValue(), currentLaneEndIndex.intValue());
        // Double currentPieceLaneLength = (myCar.getAsDouble("currentPieceLaneLength"));
        Double currentSpeed = (myCar.getAsDouble("currentSpeed"));
        printInfo("Current speed is " + currentSpeed);
        double angleDiff = angle - lastCarAngle;
        setOverTakenSwitchIndex(pieceIndex, isStraight, angle, currentLaneStartIndex.intValue(), currentLaneEndIndex.intValue(), distanceInPiece, angleDiff);
        JSONObject inFrontCar = getCloseCarInSameLane(pieceIndex, currentLaneEndIndex);

        lastCarInFront = inFrontCar;
        useTurBoToHit = false;
        if (!isStraight) {
            radius = getCurrentLaneRadius(pieceIndex, currentLaneStartIndex);
            double distanceToFrontCar = 0;
            if (inFrontCar != null) {
                distanceToFrontCar = getDistanceBetweenCarsInSameLane(inFrontCar, myCar);
                printInfo("Distance to front car " + distanceToFrontCar);
            }

            if (inFrontCar != null && Math.abs(angle) < 47 &&
                    isTurboAvailable &&
                    comparePosition(inFrontCar, myCar) > 0 &&
                    inFrontCar.getAsDouble("totalSpeed") >= myCar.getAsDouble("totalSpeed") &&
                    Math.abs(distanceToFrontCar) <= 100 &&
                    radius > 150 &&
                    bends.containsKey(inFrontCar.getInt("lastPieceIndex")) &&    //lastPieceIndex ?
                    Math.abs(inFrontCar.getAsDouble("angle")) > 30) {
                isTurboAvailable = false;
                useTurBoToHit = true;
                lastAngleDiff = angleDiff;
                lastCarAngle = angle;
                printInfo("Using turbo to hit " + distanceToFrontCar);
                carsPosition.findFirstBy("id.color", myCarColor).put("lastPieceIndex", pieceIndex);
                return new JSONObject().put("msgType", "turbo").put("data", "Pow pow pow pow pow, or your of personalized turbo message").put("gameTick", gameTick);
            }
        }
        double throttle = decideThrottle(pieceIndex, isStraight, radius, angle, distanceInPiece, currentPieceLaneLength, currentSpeed, currentLaneStartIndex.intValue(), currentLaneEndIndex.intValue());
        boolean isCloseToLaneEnd = currentPieceLaneLength - distanceInPiece < getNextTickRunningDistance(currentSpeed, myCar.getAsDouble("currentAccelerate")); //todo
        carsPosition.findFirstBy("id.color", myCarColor).put("lastPieceIndex", pieceIndex);
        if (isCloseToLaneEnd) {
            switchLane = switches.get(pieceIndex.intValue() + 1);
        } else {
            switchLane = switches.get(pieceIndex.intValue());
        }
        JSONObject switchJson = switchLane(switchLane, pieceIndex, isCloseToLaneEnd, currentLaneStartIndex, currentLaneEndIndex, gameTick);
        if (switchJson != null) {
            return switchJson;
        }

        //carTravelRoute.put(pieceIndexOfNextDifferentCurvature, currentLaneStartIndex);

        if (shouldBeTurbo(pieceIndex, distanceInPiece, currentLaneStartIndex, currentLaneEndIndex, isStraight, angle, lastAngleDiff)) {
            isTurboAvailable = false;
            return new JSONObject().put("msgType", "turbo").put("data", "Pow pow pow pow pow, or your of personalized turbo message");
        }
        return gameTick == null ? new JSONObject().put("msgType", "throttle").put("data", throttle) : new JSONObject().put("msgType", "throttle").put("data", throttle).put("gameTick", gameTick);
    }


    private boolean shouldBeTurbo(int currentPieceIndex, double distanceInPiece, int currentLaneStartIndex, int currentLaneEndIndex, boolean isStraight, double angle, double angleDiff) {
        if (isTurboAvailable) {
            if (isStraight) {
                NextTurnInfo nextTurnInfo = calculateDistanceToNextBend(currentPieceIndex, distanceInPiece, currentLaneStartIndex, currentLaneEndIndex, true);
                if (nextTurnInfo.distanceToNextPieceInDifferentCurvature > 200) {
                    return true;
                }
                Integer key = nextTurnInfo.pieceIndexOfNextDifferentCurvature;
                BendInfo bendInfo = bendEnterMap.get(key);
                return (bendInfo != null && bendInfo.radius == 200 && bendInfo.totalAngle > 45);
                //  printInfo("turbo can be use " + nextTurnInfo.distanceToNextPieceInDifferentCurvature);

            } else {
                NextTurnInfo nextTurnInfo = calculateDistanceToNextPieceInDifferentCurve(currentPieceIndex, distanceInPiece, currentLaneStartIndex, currentLaneEndIndex);
                if (bends.get(nextTurnInfo.pieceIndexOfNextDifferentCurvature) == null) {
                    NextTurnInfo nextNextTurnInfo = calculateDistanceToNextBend(nextTurnInfo.pieceIndexOfNextDifferentCurvature, 0, currentLaneStartIndex, currentLaneEndIndex, true);
                    return nextTurnInfo.distanceToNextPieceInDifferentCurvature < 30 && angle * angleDiff < 0 && angle < 55 && nextNextTurnInfo.distanceToNextPieceInDifferentCurvature > 150 && bends.get(nextNextTurnInfo.pieceIndexOfNextDifferentCurvature).getAsDouble("radius") <= 70;
                } else {
                    return false;
                }
            }
        } else {
            return false;
        }

    }


    private int getIndexDistance(int index1, int index2) {
        if (index1 - index2 < 0) {
            return pieces.size() - index1 + index1 + 1;
        } else {
            return index1 - index2;
        }
    }

    private double maxiMumBendEnteringSpeed(int currentPieceIndex, int currentLaneStartIndex, int currentLaneEndIndex, double currentRadius, double currentSpeed, int bendIndex, int iteratorNum, double angleOfCar, double currentAngle/*currentbend angle*/) {
        if (bends.get(bendIndex) == null) {    //considering the next bend   bendIndex is straight
            if (iteratorNum == 0) {
                return currentSpeed + 1;
            }
            printInfo("next bend in different curvature's radius 0 , piece index is " + bendIndex);
            NextTurnInfo nextTurnInfo = calculateDistanceToNextBend(bendIndex, 0, currentLaneStartIndex, currentLaneEndIndex, true);
            printInfo(" next next turn distance is " + nextTurnInfo.distanceToNextPieceInDifferentCurvature);
            if (nextTurnInfo.distanceToNextPieceInDifferentCurvature < 160) {
                double maximumBendEnteringSpeed = maxiMumBendEnteringSpeed(bendIndex, currentLaneStartIndex, currentLaneEndIndex, 0, currentSpeed, nextTurnInfo.pieceIndexOfNextDifferentCurvature, iteratorNum - 1, 0, 0);
                return Math.sqrt(2 * FRACTION * nextTurnInfo.distanceToNextPieceInDifferentCurvature + maximumBendEnteringSpeed * maximumBendEnteringSpeed) - 0.01;
            } else {
                return currentSpeed + 1;
            }

            //  printInfo("return max bend entering speed a");

        }

        Double bendRadius = bends.get(bendIndex).getAsDouble("radius");
        double initialRadius = bendRadius;
        //   Double bendAngle = bends.get(bendIndex).getAsDouble("angle");
        printInfo("next bend in different curvature's radius " + bendRadius + " " + bendIndex);
        //check if there is switch between
        Integer switchKey = switches.floorKey(bendIndex);
        Integer targetLaneIndex = currentLaneEndIndex;
        if (switchKey != null && switchKey >= currentPieceIndex && switchKey < bendIndex) {   //todo check the random switch
            targetLaneIndex = shortestPath.get(switchKey);
            if (targetLaneIndex == null) {
                targetLaneIndex = currentLaneEndIndex;
            }
        }
        Double angleInBend = bends.get(bendIndex).getAsDouble("angle");
        if (angleInBend > 0) {
            bendRadius -= ((JSONObject) lanes.get(targetLaneIndex)).getAsDouble("distanceFromCenter");
        } else {
            bendRadius += ((JSONObject) lanes.get(targetLaneIndex)).getAsDouble("distanceFromCenter");
        }
        // printInfo("bendRadius " + bendRadius);
        NextTurnInfo nextTurnInfo = calculateDistanceToNextPieceInDifferentCurve(bendIndex, 0, currentLaneStartIndex, currentLaneEndIndex);

        double maximumBendEnteringSpeed2 = Math.sqrt(bendRadius * getCoherenceForSpeed(bendIndex, targetLaneIndex)); // maximumBendEnteringSpeed2 = Math.sqrt((bendRadius + 20 * Math.sin(Math.abs(angleOfCar) * Math.PI / 180) * (angleOfCar * angleInBend <=0 ? 0 : -1) ) * getCoherenceForSpeed(bendIndex, targetLaneIndex));//Math.sqrt(bendRadius * getCoherenceForSpeed(bendRadius));//Math.sqrt((bendRadius + 20 * Math.tan(59 * Math.PI / 180)) * getCoherenceForSpeed(bendRadius)); //20 * Math.sin(angleOfCar * Math.PI / 180) * (angleOfCar * angleInBend <= 0 ? 1 : -1)
        printInfo("using coherence " + getCoherenceForSpeed(bendIndex, targetLaneIndex));


        if (iteratorNum == 0) {
            printInfo("return max bend entering speed c");
            return maximumBendEnteringSpeed2;
        }

        if (nextTurnInfo.pieceIndexOfNextDifferentCurvature - bendIndex == 1 || (bendIndex == pieces.size() - 1 && nextTurnInfo.pieceIndexOfNextDifferentCurvature == 0)) {
            double maximumBendEnteringSpeedNext = maxiMumBendEnteringSpeed(bendIndex, currentLaneStartIndex, currentLaneEndIndex, 0, currentSpeed, nextTurnInfo.pieceIndexOfNextDifferentCurvature, iteratorNum - 1, 0, angleInBend);
            double maximumBendEnteringSpeed = Math.sqrt(2 * FRACTION * nextTurnInfo.distanceToNextPieceInDifferentCurvature + maximumBendEnteringSpeedNext * maximumBendEnteringSpeedNext) - 0.01;
            // printInfo("return max bend entering speed b");
            return Math.min(maximumBendEnteringSpeed, maximumBendEnteringSpeed2);
        } else {
                   /* if (angleOfCar * bendAngle < 0) {
                         maximumBendEnteringSpeed2 = maximumBendEnteringSpeed2 + Math.cos(angleOfCar * Math.PI / 180) + 0.18;
                    } else {
                        maximumBendEnteringSpeed2 = maximumBendEnteringSpeed2 - Math.cos(angleOfCar * Math.PI / 180) + 0.18;
                     }
                    printInfo("angle " + Math.sin(angleOfCar * Math.PI / 180) + " coherence " + getCoherenceForSpeed(bendRadius) + " radius " + bendRadius + " maximumBendEnteringSpeed2  " + maximumBendEnteringSpeed2);
                     if (nextTurnInfo.distanceToNextPieceInDifferentCurvature < 50) {
                        printInfo("This is short bend using the max plus 4");
                        maximumBendEnteringSpeed2 += 4;
                    }    */

            return maximumBendEnteringSpeed2;
        }
    }


    private double angleDiffDecreaseFunction(double angleDiff, int tick, double diffDecreaseFactor) {
        return angleDiff + diffDecreaseFactor * tick;
    }

    private int getPieceIndex(int currentPieceIndex, int currentLaneIndex, double distanceInPiece, double tick) {
        double speed = myCar.getAsDouble("currentSpeed");
        double accelerate = myCar.getAsDouble("currentAccelerate");
        double distance = speed * tick + accelerate * tick * tick / 2;
        double distanceLeftInCurrentPiece = getLaneLength(currentPieceIndex, currentLaneIndex, currentLaneIndex) - distanceInPiece;
        if (distanceLeftInCurrentPiece >= distance) {
            return currentPieceIndex;
        }
        for (int i = 1; i < pieces.size(); i++) {
            distanceLeftInCurrentPiece += getLaneLength(currentPieceIndex + i >= pieces.size() ? 0 : currentPieceIndex + i, currentLaneIndex, currentLaneIndex);
            if (distanceLeftInCurrentPiece >= distance) {
                return currentPieceIndex + i > pieces.size() ? 0 : currentPieceIndex + i;
            }
        }
        return currentPieceIndex;
    }

    private double calculateMaxSlipAngle(double currentAngle, double currentAngleDiff, int numOfTick, double diffDecreaseFactor, double currentSpeed, double distanceToBendInDifferentDirection, int currentPieceIndex, int currentLaneIndex, double radius, double distanceInPiece) {
        double initDiff = diffDecreaseFactor;
        double totalSlipAngleIncrease = 0;
        for (int i = 0; i < numOfTick; i++) {
            diffDecreaseFactor -= (diffDecreaseFactor > 0 ? 0.01 : -0.01);
            totalSlipAngleIncrease += angleDiffDecreaseFunction(currentAngleDiff, i, diffDecreaseFactor);
            if (Math.abs(currentAngle + totalSlipAngleIncrease) > 62 && currentSpeed * i < distanceToBendInDifferentDirection) {
                printInfo("Need drop throttle, total slip angle " + totalSlipAngleIncrease + " currentAngleDiff " + currentAngleDiff + " diffDecreaseFactor " + initDiff);
                return currentAngle + totalSlipAngleIncrease;
            }
        }
        printInfo("total slip angle " + totalSlipAngleIncrease + " currentAngleDiff " + currentAngleDiff + " diffDecreaseFactor " + initDiff);
        return currentAngle + totalSlipAngleIncrease;
    }


    private int getNextSwitchIndex(int currentPieceIndex) {
        Integer nextSwitchIndex = switches.ceilingKey(currentPieceIndex);
        if (nextSwitchIndex == null) {
            nextSwitchIndex = switches.firstKey();
        }
        return nextSwitchIndex;
    }

    private int comparePosition(JSONObject car1, JSONObject car2) {
        int car1PieceIndex = ((Double) car1.getObject("piecePosition.pieceIndex")).intValue();
        int car2PieceIndex = ((Double) car2.getObject("piecePosition.pieceIndex")).intValue();
        if (car1PieceIndex == car2PieceIndex) {
            //   printInfo("car1 index diff is " + (car1PieceIndex - car2PieceIndex));
            if (((Double) car1.getObject("piecePosition.inPieceDistance")) > ((Double) car2.getObject("piecePosition.inPieceDistance"))) {
                return 1;
            } else if (((Double) car1.getObject("piecePosition.inPieceDistance")).doubleValue() == ((Double) car2.getObject("piecePosition.inPieceDistance")).doubleValue()) {
                return 0;
            } else {
                return -1;
            }
        } else {
            if (car1PieceIndex > car2PieceIndex && (car1PieceIndex - car2PieceIndex) <= pieces.size() / 2) {
                printInfo("car1 index diff is " + (car1PieceIndex - car2PieceIndex));
                return 1;
            } else if (car1PieceIndex > car2PieceIndex && (car1PieceIndex - car2PieceIndex) > pieces.size() / 2) {
                printInfo("car1 index diff is " + (car1PieceIndex - car2PieceIndex));
                return -1;
            } else if (car1PieceIndex < car2PieceIndex && (car2PieceIndex - car1PieceIndex > pieces.size() / 2)) {
                printInfo("car1 index diff is " + (car1PieceIndex - car2PieceIndex));
                return 1;
            } else {
                printInfo("car1 index diff is " + (car1PieceIndex - car2PieceIndex));
                return -1;
            }
        }
    }

    private boolean isFaster(JSONObject car1, JSONObject car2) {
        return (Double) car1.getObject("totalSpeed", 0d) >= (Double) car2.getObject("totalSpeed", 0d) && ((Double) car1.getObject("currentSpeed", 0d) >= (Double) car2.getObject("currentSpeed", 0));
    }


    private boolean isTwoCarFarEnough(JSONObject car1, JSONObject car2) {
        int car1PieceIndex = ((Double) car1.getObject("piecePosition.pieceIndex")).intValue();
        int car2PieceIndex = ((Double) car2.getObject("piecePosition.pieceIndex")).intValue();
        int minIndex = Math.min(car1PieceIndex, car2PieceIndex);
        int maxIndex = Math.max(car1PieceIndex, car2PieceIndex);
        if (minIndex >= 0 && minIndex < 3 && maxIndex >= pieces.size() - 2) {
            return false;
        }
        return Math.abs(maxIndex - minIndex) >= 5;
    }

    protected boolean isLaneFrontEmpty(int currentLaneIndex) {
        if (currentLaneIndex == ((Double) myCar.getObject("piecePosition.lane.endLaneIndex")).intValue()) {
            double speedDiff = myCar.getAsDouble("currentSpeed") - myCar.getAsDouble("lastTickSpeed");
            if (myCar.getAsDouble("currentSpeed") - myCar.getAsDouble("lastTickSpeed") < 0 && Math.abs(speedDiff) / myCar.getAsDouble("lastTickSpeed") > 0.5 && !isCrash) {
                printInfo("There is possible crash");
                havingCrashLaneIndex = currentLaneIndex;
                havingCrashPieceIndex = ((Double) myCar.getObject("piecePosition.pieceIndex")).intValue();
                return false;
            }
        }
        boolean isEmptyInFront = true;
        for (Object obj : carsPosition) {
            JSONObject car = (JSONObject) obj;
            if (!car.getObject("id.color").equals(myCarColor) && ((Double) car.getObject("piecePosition.lane.endLaneIndex")).intValue() == currentLaneIndex) {   //todo: switch?
                boolean isMyCarInFrontIt = comparePosition(myCar, car) > 0;
                boolean isTwoCarFar = isTwoCarFarEnough(myCar, car);
                boolean IsFasterThanMe = isFaster(car, myCar);
                printInfo(isMyCarInFrontIt + " " + isTwoCarFar + " " + IsFasterThanMe);
                isEmptyInFront &= isMyCarInFrontIt || isTwoCarFar || IsFasterThanMe;
            }
        }

        return isEmptyInFront;

    }

    private double getDistanceInDifferentPiece(int car1PieceIndex, int car2PieceIndex, int laneIndex, JSONObject car1, JSONObject car2) { //car1 index > car 2 index
        double distance = getLaneLength(car2PieceIndex, laneIndex, laneIndex) - ((Double) car2.getObject("piecePosition.inPieceDistance")); //todo switch?
        for (int startIndex = car2PieceIndex + 1; startIndex <= car1PieceIndex; startIndex++) {
            if (startIndex == car1PieceIndex) {
                distance += ((Double) car1.getObject("piecePosition.inPieceDistance"));
            } else {
                distance += getLaneLength(startIndex, laneIndex, laneIndex);
            }
        }
        return distance;
    }

    private double getDistanceBetweenCarsInSameLane(JSONObject car1, JSONObject car2) {
        int car1PieceIndex = ((Double) car1.getObject("piecePosition.pieceIndex")).intValue();
        int car2PieceIndex = ((Double) car2.getObject("piecePosition.pieceIndex")).intValue();
        int laneIndex = ((Double) car1.getObject("piecePosition.lane.startLaneIndex")).intValue();
        double distance = 0;
        if (car1PieceIndex == car2PieceIndex) {
            return (Double) car1.getObject("piecePosition.inPieceDistance") - (Double) car2.getObject("piecePosition.inPieceDistance");
        } else if (car1PieceIndex > car2PieceIndex && (car1PieceIndex - car2PieceIndex) <= pieces.size() / 2) {
            return getDistanceInDifferentPiece(car1PieceIndex, car2PieceIndex, laneIndex, car1, car2);
        } else if (car1PieceIndex > car2PieceIndex && (car1PieceIndex - car2PieceIndex) > pieces.size() / 2) {
            distance = getLaneLength(car1PieceIndex, laneIndex, laneIndex) - ((Double) car1.getObject("piecePosition.inPieceDistance")); //todo switch?
            for (int startIndex = car1PieceIndex + 1; startIndex <= pieces.size() - 1; startIndex++) {
                distance += getLaneLength(startIndex, laneIndex, laneIndex);
            }
            for (int startIndex = 0; startIndex < car2PieceIndex; startIndex++) {
                distance += getLaneLength(startIndex, laneIndex, laneIndex);
            }
            return distance + ((Double) car2.getObject("piecePosition.inPieceDistance"));
        } else if (car1PieceIndex < car2PieceIndex && (car2PieceIndex - car1PieceIndex > pieces.size() / 2)) {
            distance = getLaneLength(car2PieceIndex, laneIndex, laneIndex) - ((Double) car2.getObject("piecePosition.inPieceDistance")); //todo switch?
            for (int startIndex = car2PieceIndex + 1; startIndex <= pieces.size() - 1; startIndex++) {
                distance += getLaneLength(startIndex, laneIndex, laneIndex);
            }
            for (int startIndex = 0; startIndex < car1PieceIndex; startIndex++) {
                distance += getLaneLength(startIndex, laneIndex, laneIndex);
            }
            return distance + ((Double) car1.getObject("piecePosition.inPieceDistance"));
        } else {
            return getDistanceInDifferentPiece(car2PieceIndex, car1PieceIndex, laneIndex, car2, car1);
        }

    }

    private boolean isPieceInRange(List<Integer> pieceRange, int targetPieceIndex) {
        for (Integer pieceIndex : pieceRange) {
            if (pieceIndex.intValue() == targetPieceIndex) {
                return true;
            }
        }
        return false;
    }

    boolean isPieceStraight(int currentPieceIndex) {
        return ((JSONObject) pieces.get(currentPieceIndex)).contains("length");
    }

    boolean isPieceStraight(JSONObject piece) {
        return piece.contains("length");
    }

    private JSONObject getSlowestCarInFront(int currentPieceIndex, int currentLaneIndex) {
        //printInfo("Current piece index is " + currentPieceIndex);
        List<Integer> pieceRange = new ArrayList<>();
        if (currentPieceIndex + 3 < pieces.size()) {
            pieceRange.add(currentPieceIndex);
            pieceRange.add(currentPieceIndex + 1);
            pieceRange.add(currentPieceIndex + 2);
            pieceRange.add(currentPieceIndex + 3);
        } else {
            pieceRange.add(currentPieceIndex);
            pieceRange.add(Math.abs(currentPieceIndex + 1 - pieceRange.size()));
            pieceRange.add(Math.abs(currentPieceIndex + 2 - pieceRange.size()));
            pieceRange.add(Math.abs(currentPieceIndex + 3 - pieceRange.size()));
        }

        double minimumSpeed = 1000d;
        JSONObject slowestCar = null;

        for (Object obj : carsPosition) {
            JSONObject car = (JSONObject) obj;
            if (currentPieceIndex == ((Double) car.getObject("piecePosition.pieceIndex")).intValue() && (Double) myCar.getObject("piecePosition.inPieceDistance") >= (Double) car.getObject("piecePosition.inPieceDistance")) {
                continue;
            }
            if (!car.getObject("id.color").equals(myCarColor) &&
                    ((Double) car.getObject("piecePosition.lane.endLaneIndex")).intValue() == currentLaneIndex
                    && isPieceInRange(pieceRange, ((Double) car.getObject("piecePosition.pieceIndex")).intValue())) {   //todo: switch?
                double speed = (Double) car.getObject("currentSpeed");
                //printInfo("Distance to " + car.getObject("id.color") + " is " + distance);
                if (speed < minimumSpeed) {
                    minimumSpeed = speed;
                    slowestCar = car;
                    //    printInfo("Close car is " + car.getObject("id.color"));
                }
            }
        }

        if (slowestCar == null) {
            //printInfo("no slow car, target laneIndex is " + currentLaneIndex);
            return null;
        }
        return slowestCar;
    }

    private boolean isLaneHavingCarSlowerThanMe(int currentPieceIndex, int currentLaneIndex) {
        // printInfo("Current piece index is " + currentPieceIndex);
        JSONObject slowestCarInFrontOfMe = getSlowestCarInFront(currentPieceIndex, currentLaneIndex);
        JSONObject myCar = carsPosition.findFirstBy("id.color", myCarColor);

        if (slowestCarInFrontOfMe == null) {
            // printInfo("no slow car, target laneIndex is " + currentLaneIndex);
            return false;
        }
        printInfo(slowestCarInFrontOfMe.getObject("totalSpeed") + " my car is " + myCar.getObject("totalSpeed"));
        return (((Double) slowestCarInFrontOfMe.getObject("totalSpeed") < (Double) myCar.getObject("totalSpeed")) && ((Double) slowestCarInFrontOfMe.getObject("currentSpeed") < (Double) myCar.getObject("currentSpeed"))) || (havingCrashLaneIndex == currentLaneIndex);
    }

    private void setOverTaken(String direction, int switchIndex) {
        overTakenDirection = direction;
        overTakenSwitchIndex = switchIndex;
        printInfo("Set over taken direction " + direction + " overTakenSwitchIndex " + switchIndex);
    }


    private void checkOverTakenLane(int pieceIndex, int currentLaneStartIndex, int nextSwitchNext) {
        //  printInfo("Car in front is slower than me");
        if (currentLaneStartIndex == lanes.size() - 1) {
            if (isLaneFrontEmpty(Integer.valueOf(currentLaneStartIndex - 1))) {
                setOverTaken("Left", nextSwitchNext);
            } else {
                JSONObject slowCarInOtherLane = getSlowestCarInFront(pieceIndex, currentLaneStartIndex - 1);
                JSONObject slowestCarInFrontOfMe = getSlowestCarInFront(pieceIndex, currentLaneStartIndex);
                if (slowCarInOtherLane == null || isFaster(slowCarInOtherLane, slowestCarInFrontOfMe)) {
                    setOverTaken("Left", nextSwitchNext);
                }
            }
            return;
        }

        if (currentLaneStartIndex == 0) {
            if (isLaneFrontEmpty(Integer.valueOf(currentLaneStartIndex + 1))) {
                setOverTaken("Right", nextSwitchNext);
            } else {
                JSONObject slowCarInOtherLane = getSlowestCarInFront(pieceIndex, currentLaneStartIndex + 1);
                JSONObject slowestCarInFrontOfMe = getSlowestCarInFront(pieceIndex, currentLaneStartIndex);
                if (slowCarInOtherLane == null || isFaster(slowCarInOtherLane, slowestCarInFrontOfMe)) {
                    setOverTaken("Right", nextSwitchNext);
                }
            }
            return;
        }

        double nextBendAngle = getNextBendIndex(pieceIndex);

        if (nextBendAngle > 0) {
            if (isLaneFrontEmpty(Integer.valueOf(currentLaneStartIndex + 1))) {
                setOverTaken("Right", nextSwitchNext);
                return;
            } else {
                if (isLaneFrontEmpty(Integer.valueOf(currentLaneStartIndex - 1))) {
                    setOverTaken("Left", nextSwitchNext);
                    return;
                }
                JSONObject slowCarInOtherLane1 = getSlowestCarInFront(pieceIndex, currentLaneStartIndex + 1);
                JSONObject slowCarInOtherLane2 = getSlowestCarInFront(pieceIndex, currentLaneStartIndex - 1);
                JSONObject slowestCarInFrontOfMe = getSlowestCarInFront(pieceIndex, currentLaneStartIndex);
                if (slowCarInOtherLane1 == null || (isFaster(slowCarInOtherLane1, slowestCarInFrontOfMe) && isFaster(slowCarInOtherLane1, slowCarInOtherLane2))) {
                    setOverTaken("Right", nextSwitchNext);
                } else if (slowCarInOtherLane2 == null || (isFaster(slowCarInOtherLane2, slowestCarInFrontOfMe) && isFaster(slowCarInOtherLane2, slowCarInOtherLane1))) {
                    setOverTaken("Left", nextSwitchNext);
                }

                return;

            }


        } else {
            if (isLaneFrontEmpty(Integer.valueOf(currentLaneStartIndex - 1))) {
                setOverTaken("Left", nextSwitchNext);
                return;
            } else {
                if (isLaneFrontEmpty(Integer.valueOf(currentLaneStartIndex + 1))) {
                    setOverTaken("Right", nextSwitchNext);
                    return;
                }
                JSONObject slowCarInOtherLane1 = getSlowestCarInFront(pieceIndex, currentLaneStartIndex - 1);
                JSONObject slowCarInOtherLane2 = getSlowestCarInFront(pieceIndex, currentLaneStartIndex + 1);
                JSONObject slowestCarInFrontOfMe = getSlowestCarInFront(pieceIndex, currentLaneStartIndex);
                if (slowCarInOtherLane1 == null || (isFaster(slowCarInOtherLane1, slowestCarInFrontOfMe) && isFaster(slowCarInOtherLane1, slowCarInOtherLane2))) {
                    setOverTaken("Left", nextSwitchNext);
                } else if (slowCarInOtherLane2 == null || (isFaster(slowCarInOtherLane2, slowestCarInFrontOfMe) && isFaster(slowCarInOtherLane2, slowCarInOtherLane1))) {
                    setOverTaken("Right", nextSwitchNext);
                }
                return;

            }
        }
    }


    private void setOverTakenSwitchIndex(int pieceIndex, boolean isStraight, double angle, int currentLaneStartIndex, int currentLaneEndIndex, double distanceInPiece, double angleDiff) {

        int nextSwitchNext = getNextSwitchIndex(pieceIndex);
        // printInfo("Next possible switch is " + nextSwitchNext);
        boolean isTurboAvailable = shouldBeTurbo(nextSwitchNext, 0, currentLaneStartIndex, currentLaneEndIndex, bends.get(nextSwitchNext) == null, angle, angleDiff);
        if (isLaneFrontEmpty(currentLaneStartIndex) && !isTurboAvailable) {
            overTakenSwitchIndex = -1;
            // printInfo("Lane in front is empty.");
            return;
        }
        if (isLaneHavingCarSlowerThanMe(pieceIndex, currentLaneStartIndex) || (isTurboAvailable && getSlowestCarInFront(pieceIndex, currentLaneEndIndex) != null)) {
            checkOverTakenLane(pieceIndex, currentLaneStartIndex, nextSwitchNext);
        } else {
            overTakenSwitchIndex = -1;
        }

    }


    private JSONObject getCloseCarInSameLane(int currentPieceIndex, int laneIndex) {
        //printInfo("Current piece index is " + currentPieceIndex);
        List<Integer> pieceRange = new ArrayList<>();
        if (currentPieceIndex + 1 < pieces.size()) {
            pieceRange.add(currentPieceIndex);
            pieceRange.add(currentPieceIndex + 1);
        } else {
            pieceRange.add(currentPieceIndex);
            pieceRange.add(Math.abs(currentPieceIndex + 1 - pieceRange.size()));
        }

        double minimumDistance = 1000d;
        JSONObject closestCar = null;
        for (Object obj : carsPosition) {
            JSONObject car = (JSONObject) obj;
            int carIndex = ((Double) car.getObject("piecePosition.pieceIndex")).intValue();
            if (!car.getObject("id.color").equals(myCarColor) &&
                    ((Double) car.getObject("piecePosition.lane.endLaneIndex")).intValue() == laneIndex
                    && isPieceInRange(pieceRange, carIndex) && comparePosition(car, myCar) > 0) {
                double distance = getDistanceBetweenCarsInSameLane(car, car);
                if (distance < minimumDistance) {
                    minimumDistance = distance;
                    closestCar = car;
                }
            }
        }
        return closestCar;
    }


    int getTickToCover(double distance, double currentSpeed, double accelerate) {
        double rightRoot = 0;
        //  rightRoot = (currentSpeed * (-1) + Math.sqrt(Math.pow(currentSpeed, 2) + 2 * accelerate * accelerate * distance * (accelerate > 0 ? 1 : -1))) / (accelerate * accelerate * (accelerate > 0 ? 1 : -1));
        return Double.valueOf(Math.ceil(distance / currentSpeed)).intValue(); //Double.valueOf(rightRoot).intValue() + 1;

    }

    private double decideThrottle(int pieceIndex, boolean isStraight, double radius, double angle, double inPieceDistance, double laneLength, double currentSpeed, int currentLaneStartIndex, int currentLaneEndIndex) {
        // printInfo("End line index is " + currentLaneEndIndex);
        double angleDiff = angle - lastCarAngle;
        double deltaAngleDiff = angleDiff - lastAngleDiff;
        lastAngleDiff = angleDiff;
        lastCarAngle = angle;
        printInfo("radius is " + radius);
        if (isStraight) {
            lastThrottle = decideThrottleForStraight(pieceIndex, angleDiff, deltaAngleDiff, radius, angle, inPieceDistance, laneLength, currentSpeed, currentLaneStartIndex, currentLaneEndIndex);
        } else {
            if (lapCount >= 1 && currentSpeed < minimumBendSpeedAll) {
                minimumBendSpeedAll = currentSpeed;
            }
            lastThrottle = decideThrottleForBend(pieceIndex, angleDiff, deltaAngleDiff, radius, angle, inPieceDistance, laneLength, currentSpeed, currentLaneStartIndex, currentLaneEndIndex, myCar.getAsDouble("currentAccelerate"));
        }
        return lastThrottle;
    }

    private double decideThrottleForStraight(int pieceIndex, double angleDiff, double deltaAngleDiff, double radius, double angle, double inPieceDistance, double laneLength, double currentSpeed, int currentLaneStartIndex, int currentLaneEndIndex) {
        NextTurnInfo nextTurnInfo = calculateDistanceToNextBend(pieceIndex, inPieceDistance, currentLaneStartIndex, currentLaneEndIndex, true);
        // printInfo("Next turn piece index straight " + nextTurnInfo.pieceIndexOfNextDifferentCurvature);
        int tickToFinnishCurrentDirectionBend = Double.valueOf(Math.ceil(nextTurnInfo.distanceToNextPieceInDifferentCurvature / currentSpeed)).intValue() + 1;
        double currentAngle = 0;
        double maxSlipAngle = Math.abs((angle != 0) ? calculateMaxSlipAngle(angle, angleDiff, 7, deltaAngleDiff, currentSpeed, nextTurnInfo.distanceToNextPieceInDifferentCurvature, pieceIndex, currentLaneEndIndex, radius, inPieceDistance) : 0);
        double throttle;
        BendEnteringInfo bendEnteringInfo = throttleZero(currentSpeed, nextTurnInfo.distanceToNextPieceInDifferentCurvature, pieceIndex, currentLaneStartIndex, currentLaneEndIndex, 0, nextTurnInfo.pieceIndexOfNextDifferentCurvature, angle, currentAngle);
        if ((maxSlipAngle > 50 && angle * angleDiff > 0) || maxSlipAngle > 60) {
            throttle = 0d;
        } else if (bendEnteringInfo.shouldBreak) {
            if (Math.abs(currentSpeed - bendEnteringInfo.maxBendEnteringSpeed) <= 0.02) {
                throttle = 0.3d;
            } else {
                throttle = 0d;
            }
        } else if (bendEnteringInfo.maxBendEnteringSpeed > currentSpeed && bendEnteringInfo.maxBendEnteringSpeed - currentSpeed < 0.08) {
            throttle = currentSpeed / 10 - 0.03;
        } else {
            throttle = 1d;
        }
        return throttle;
    }


    private double decideThrottleForBend(int pieceIndex, double angleDiff, double deltaAngleDiff, double radius, double angle, double inPieceDistance, double laneLength, double currentSpeed, int currentLaneStartIndex, int currentLaneEndIndex, double speedDiff) {
        NextTurnInfo nextTurnInfo = calculateDistanceToNextPieceInDifferentCurve(pieceIndex, inPieceDistance, currentLaneStartIndex, currentLaneEndIndex);
        printInfo("Distance to next piece in different direction bends is " + nextTurnInfo.distanceToNextPieceInDifferentDirection + " index is " + nextTurnInfo.pieceIndexOfNextDifferentDirection);
        printInfo("Distance to next piece in different curvature bends is " + nextTurnInfo.distanceToNextPieceInDifferentCurvature + " index is " + nextTurnInfo.pieceIndexOfNextDifferentCurvature);
        printInfo("Current lane length " + laneLength);
        int bendStartIndex = bendEnterMap.floorKey(pieceIndex);

        if (currentSpeed == 0) {
            return 1d;
        }
        JSONObject currentBend = bends.get(pieceIndex);
        double currentBendAngle = currentBend.getAsDouble("angle");
        int lastIndex = myCar.getInt("lastPieceIndex");
        JSONObject lastBend = bends.get(lastIndex);
        JSONObject nextBend = bends.get(nextTurnInfo.pieceIndexOfNextDifferentCurvature);

        if (lastIndex != pieceIndex && bendEnterMap.floorKey(lastIndex) != bendEnterMap.floorKey(pieceIndex)) {
            maxBendSpeed = 0;
            maxBendAngle = 0;
            isCoherenceChange = false;
            hasCoherenceChangedByAngle = false;
            oldCoherenceInBend = getCoherenceForSpeed(bendStartIndex, currentLaneStartIndex);
            if (lastBend == null || lastBend.getAsDouble("angle") * currentBend.getAsDouble("angle") < 0) {
                deltaAngleDiff = 0d;
            }
            if (lastThrottle != 1 && angle * currentBend.getAsDouble("angle") >= 0) {
                return lastThrottle;
            }

        }

        if (currentSpeed > maxBendSpeed) {
            maxBendSpeed = currentSpeed;
            double newCoherenceForSpeed = currentSpeed * currentSpeed / ((Math.sin(Math.abs(angle) * Math.PI / 180) * 20 + radius));   // radius;//

            if (newCoherenceForSpeed > getCoherenceForSpeed(bendStartIndex, currentLaneStartIndex)) {                 //
                bendEnterMap.get(bendStartIndex).coherenceSpeeds.set(currentLaneStartIndex, newCoherenceForSpeed);
                coherenceForSpeed = newCoherenceForSpeed;
                isCoherenceChange = true;
            }
        }

        if (Math.abs(angle) > maxBendAngle) {
            maxBendAngle = Math.abs(angle);
        }

        if (nextTurnInfo.distanceToNextPieceInDifferentCurvature < 7 && isCoherenceChange /*(lapCount != 0 || bendEnterMap.floorKey(pieceIndex) != bendEnterMap.firstKey())*/) {
            if (nextBend != null && nextBend.getAsDouble("angle") * currentBend.getAsDouble("angle") < 0) {
                if (!hasCoherenceChangedByAngle) {
                    printInfo("modify the coherence because of low angle");
                    double newCoherenceForSpeed = bendEnterMap.get(bendStartIndex).coherenceSpeeds.get(currentLaneStartIndex);
                    if (maxBendAngle < 50) {
                        newCoherenceForSpeed += 0.03;
                        bendEnterMap.get(bendStartIndex).coherenceSpeeds.set(currentLaneStartIndex, newCoherenceForSpeed);
                        hasCoherenceChangedByAngle = true;
                    }
                    coherenceForSpeed = newCoherenceForSpeed;
                } else {
                    double newCoherenceForSpeed = bendEnterMap.get(bendStartIndex).coherenceSpeeds.get(currentLaneStartIndex);
                    if (maxBendAngle > 50) {
                        newCoherenceForSpeed -= 0.03;
                        bendEnterMap.get(bendStartIndex).coherenceSpeeds.set(currentLaneStartIndex, newCoherenceForSpeed);
                        hasCoherenceChangedByAngle = false;
                    }
                }
            } else if (currentBend.getAsDouble("radius") == 50 && ((maxBendAngle > 38 && Math.abs(angleDiff) > 4) || (maxBendAngle > 48 && Math.abs(angleDiff) > 2))) {
                printInfo("using old coherence " + oldCoherenceInBend);
                bendEnterMap.get(bendStartIndex).coherenceSpeeds.set(currentLaneStartIndex, oldCoherenceInBend);
            }
        }

        printInfo("Coherence is " + getCoherenceForSpeed(pieceIndex, currentLaneStartIndex));

        int tickToFinnishCurrentDirectionBend = getTickToCover(nextTurnInfo.distanceToNextPieceInDifferentDirection, currentSpeed, speedDiff);
        int originalTick = tickToFinnishCurrentDirectionBend;
        printInfo("tickToFinnishCurrentDirectionBend " + tickToFinnishCurrentDirectionBend + " distance to cover " + nextTurnInfo.distanceToNextPieceInDifferentDirection);

        if (trackName.equals("keimola")) {
            if (tickToFinnishCurrentDirectionBend <= 6 && Math.abs(angleDiff) > 3.3 && angleDiff * angle > 0) {
                //    printInfo(" modify tick to finish " + (tickToFinnishCurrentDirectionBend + 1));
                tickToFinnishCurrentDirectionBend = tickToFinnishCurrentDirectionBend + 2;
            } else if (tickToFinnishCurrentDirectionBend <= 7) {
                tickToFinnishCurrentDirectionBend = tickToFinnishCurrentDirectionBend + 1;
            } else if (tickToFinnishCurrentDirectionBend > 12) {
                tickToFinnishCurrentDirectionBend = 12;
                //   printInfo(" modify tick to finish " + 12);
            }
        } else {
            if (currentBend.getAsDouble("radius") == 50.0) {
                //    printInfo(" modify tick to finish " + (tickToFinnishCurrentDirectionBend + 1));
                // JSONObject bend = bends.get(nextTurnInfo.distanceToNextPieceInDifferentDirection);
               /* if (bend != null && bend.getAsDouble("angle") * bends.get(pieceIndexOfNextDifferentCurvature).getAsDouble("angle") > 0 && Math.abs(bend.getAsDouble("radius") - radius) > 140) {  //from small bend to big bend
                     tickToFinnishCurrentDirectionBend = 12;
                    printInfo(" modify tick to finish " + tickToFinnishCurrentDirectionBend);
                } else if ((bend == null || (currentBend != null && bend.getAsDouble("angle") * currentBend.getAsDouble("angle") > 0))  && Math.abs(angleDiff) > 3.3 && angleDiff * angle > 0) {
                     tickToFinnishCurrentDirectionBend = 11;
                }*/

                if (Math.abs(angleDiff) > 2 && Math.abs(bendEnterMap.floorEntry(pieceIndex).getValue().totalAngle) > 45/*&& (bend == null || (bend.getAsDouble("angle") * currentBend.getAsDouble("angle") > 0))*/) {
                    tickToFinnishCurrentDirectionBend += 6;
                    printInfo(" modify tick to finish 6 plus");
                } else if (Math.abs(bendEnterMap.floorEntry(pieceIndex).getValue().totalAngle) > 45 && tickToFinnishCurrentDirectionBend <= 11) {
                    tickToFinnishCurrentDirectionBend = tickToFinnishCurrentDirectionBend + 3;
                }

            } else {

                /*if (nextBend != null && nextBend.getAsDouble("angle") * bends.get(pieceIndexOfNextDifferentCurvature).getAsDouble("angle") > 0) {  //from small bend to big bend
                    tickToFinnishCurrentDirectionBend = 12 ;
                }  */
                if (nextBend == null && tickToFinnishCurrentDirectionBend <= 6 && Math.abs(angleDiff) > 3.3) {
                    tickToFinnishCurrentDirectionBend = 8;
                    printInfo(" modify tick to finish " + " 8  ");
                } else if (tickToFinnishCurrentDirectionBend <= 10) {
                    tickToFinnishCurrentDirectionBend = tickToFinnishCurrentDirectionBend + 2;
                } else if (tickToFinnishCurrentDirectionBend > 12) {
                    tickToFinnishCurrentDirectionBend = 12;
                    printInfo(" modify tick to finish " + 12);
                }
             /*   if (tickToFinnishCurrentDirectionBend <= 2) {
                    //    printInfo(" modify tick to finish " + (tickToFinnishCurrentDirectionBend + 1));
                    tickToFinnishCurrentDirectionBend = tickToFinnishCurrentDirectionBend + 1;
                } else if (tickToFinnishCurrentDirectionBend > 12) {
                    tickToFinnishCurrentDirectionBend = 12;
                    //   printInfo(" modify tick to finish " + 12);
                }*/
            }

        }


        double maxSlipAngle = calculateMaxSlipAngle(angle, angleDiff, tickToFinnishCurrentDirectionBend, deltaAngleDiff, currentSpeed, nextTurnInfo.pieceIndexOfNextDifferentDirection, pieceIndex, currentLaneEndIndex, radius, inPieceDistance);
        double optionSpeed = Math.sqrt(getCoherence(radius) * (radius + Math.tan((60 - Math.abs(angle)) * Math.PI / 180) * 20)) / 10;
        //printInfo("Option speed is " + optionSpeed);
        bendEnteringInfo = throttleZero(currentSpeed, nextTurnInfo.distanceToNextPieceInDifferentCurvature, pieceIndex, currentLaneStartIndex, currentLaneEndIndex, radius, nextTurnInfo.pieceIndexOfNextDifferentCurvature, angle, currentBend.getAsDouble("angle"));
        if (bendEnteringInfo.shouldBreak) {    //check  in next turn 's entering speed
            if (Math.abs(currentSpeed - bendEnteringInfo.maxBendEnteringSpeed) <= 0.02) {
                lastThrottle = 0.3d;
            } else {
                lastThrottle = 0d;
            }
        } else if (Math.abs(maxSlipAngle) > 60 && maxSlipAngle * currentBend.getAsDouble("angle") < 0) {
            lastThrottle = 1d;
        } else if (Math.abs(maxSlipAngle) > 60 && Math.abs(angle) < 5) {
            lastThrottle = 0.25d;
        } else if (Math.abs(maxSlipAngle) > 59.7) {
            lastThrottle = 0;
        } else {
            if (!isTurboing) {
                lastThrottle = 1; //optionSpeed / 10 - 0.03;//currentSpeed / 10 + (60 - maxSlipAngle) / 25 - (trackName.equals("suzuka") || trackName.equals("germany") ? 0.3 : 0.25);
            } else {
                lastThrottle = 0.3d;
            }
        }

        if (lastThrottle > 1) {
            return 1d;
        }

        if (lastThrottle < 0) {
            return 0d;
        }
        return lastThrottle;
    }

    double getCurrentBendRangeLength(int pieceIndex, double radius, int currentLaneStartIndex, int currentLaneEndIndex) {
        int firstIndex = -1;
        for (int i = pieceIndex; i >= 0; i--) {
            if (bends.get(i) == null) {
                firstIndex = i + 1;
                break;
            }
        }
        if (firstIndex == -1) {
            for (int i = pieces.size(); i >= 0; i--) {
                if (bends.get(i) == null) {
                    firstIndex = (i == pieces.size()) ? 0 : i + 1;
                    break;
                }
            }
        }
        return firstIndex;
    }

    private void initLaneBendLength(List<Double> laneBendLength, List<Double> laneCurvature, List<Double> laneDuration) {
        laneBendLength.clear();
        laneCurvature.clear();
        laneDuration.clear();
        for (int i = 0; i < numberOfLane; i++) {
            laneBendLength.add(0d);
            laneCurvature.add(0d);
            laneDuration.add(0d);
        }
    }

    private void setCoherence() {
        for (Map.Entry<Integer, BendInfo> entry : bendEnterMap.entrySet()) {
            int startIndex = entry.getKey();
            BendInfo bendInfo = entry.getValue();
            JSONObject nextNextBend = bends.get(startIndex + bendInfo.pieceNumber);
            for (int i = 0; i < lanes.size(); i++) {
                double bendRadius = getCurrentLaneRadius(startIndex, i);
                double coherence = getCoherenceForSpeed(bendRadius);
                if (bendInfo.radius <= 50) {
                    if (Math.abs(bendInfo.totalAngle) <= 45) {
                        if ((nextNextBend != null && nextNextBend.getAsDouble("angle") * bendInfo.angle < 0)) {
                            printInfo("next next bend index " + (startIndex + bendInfo.pieceNumber) + " This is short bend using big coherence ");
                            coherence *= 2.5;
                        } else if (nextNextBend != null && nextNextBend.getAsDouble("angle") * bendInfo.angle > 0) {
                            printInfo("This is short bend using max for speed plus 1.7");
                            coherence = 1.1;
                        }
                    } else if (Math.abs(bendInfo.totalAngle) == 90) {
                        if (nextNextBend != null && nextNextBend.getAsDouble("angle") * bendInfo.angle < 0) {
                            coherence = 0.6;
                        }
                    } else if (Math.abs(bendInfo.totalAngle) == 180) {
                        if ((nextNextBend == null || nextNextBend.getAsDouble("angle") * bendInfo.angle < 0)) {
                            coherence = 0.515;
                        }
                    } else if (Math.abs(bendInfo.totalAngle) == 135) {
                        if ((nextNextBend != null && nextNextBend.getAsDouble("angle") * bendInfo.angle > 0)) {
                            coherence = 0.575;
                        } else if (nextNextBend == null) {
                            coherence = 0.515;
                        }
                    }
                }

                if (bendInfo.radius <= 100) {
                    if (Math.abs(bendInfo.totalAngle) <= 22.5) {
                        if ((nextNextBend == null && ((JSONObject) (pieces.get(startIndex + bendInfo.pieceNumber))).getAsDouble("length") > 50) || (nextNextBend != null && nextNextBend.getAsDouble("angle") * bendInfo.angle < 0)) {
                            printInfo("next next bend index " + (startIndex + bendInfo.pieceNumber) + " This is short bend using big coherence ");
                            coherence *= 2;
                        } else if (nextNextBend == null && ((JSONObject) (pieces.get(startIndex + bendInfo.pieceNumber))).getAsDouble("length") <= 50) {
                            coherence *= 1;
                        } else if (nextNextBend != null && nextNextBend.getAsDouble("angle") * bendInfo.angle > 0 && nextNextBend.getAsDouble("radius") > bendInfo.radius) {
                            printInfo("This is short bend using max for speed plus 1.7");
                            coherence = 0.8;
                        }
                    }

                    if (Math.abs(bendInfo.totalAngle) <= 90) {
                        if ((nextNextBend == null || nextNextBend.getAsDouble("angle") * bendInfo.angle < 0)) {
                            coherence = 0.56;
                        }
                    }
                }

                if (bendInfo.radius == 200) {
                    if (Math.abs(bendInfo.totalAngle) <= 45) {
                        if (nextNextBend == null || nextNextBend.getAsDouble("angle") * bendInfo.angle < 0) {
                            coherence = 0.65;
                        }
                    }
                }

                bendInfo.maximumEnteringSpeeds.add(Math.sqrt(bendRadius * coherence));
                bendInfo.coherenceSpeeds.add(coherence);
            }
        }
    }


    public void initGame(final MsgWrapper msgWrapper) {
        try {
            printInfo(msgWrapper.data.toString());
            JSONObject data = JSONObject.toJSONObject(new JSONObject(), (Map) msgWrapper.data);
            printInfo(data.toString());
            pieces = (JSONArray) data.getObject("race.track.pieces");
            lanes = (JSONArray) data.getObject("race.track.lanes");
            numberOfLane = lanes.length();
            switches.clear();
            carsPosition.clear();
            trackName = data.getObject("race.track.id", "").toString();

            List<JSONObject> bendsPiecesBeforeSwitch = new ArrayList<>();
            Double bendLengthBeforeSwitch = 0d;

            final List<Double> laneBendLength = new ArrayList<>();
            List<Double> laneCurvature = new ArrayList<>();
            List<Double> laneDuration = new ArrayList<>();
            initLaneBendLength(laneBendLength, laneCurvature, laneDuration);


            for (int i = 0; i < pieces.size(); i++) {
                JSONObject piece = (JSONObject) pieces.get(i);
                if (piece.contains("switch")) {
                    switches.put(i, piece);
                }
                if (piece.contains("radius")) {
                    bends.put(i, piece);
                }
            }

            for (int i = switches.firstKey(); i < switches.lastKey(); i = switches.higherKey(i)) {
                for (int j = i; j < switches.higherKey(i); j++) {
                    JSONObject piece = bends.get(j);
                    if (piece != null) {
                        bendsPiecesBeforeSwitch.add(piece);
                        calculateArcLength(piece, bendLengthBeforeSwitch, laneBendLength, laneCurvature, laneDuration);
                    }
                }
                if (bendsPiecesBeforeSwitch.size() == 0) {
                    calculateArcLength(null, bendLengthBeforeSwitch, laneBendLength, laneCurvature, laneDuration);
                }
                setBendInfo(bendsPiecesBeforeSwitch, bendLengthBeforeSwitch, laneBendLength, i, laneCurvature, laneDuration);
            }

            for (int j = switches.lastKey(); j < pieces.size(); j++) {
                JSONObject piece = bends.get(j);
                if (piece != null) {
                    bendsPiecesBeforeSwitch.add(piece);
                    calculateArcLength(piece, bendLengthBeforeSwitch, laneBendLength, laneCurvature, laneDuration);
                }
            }

            for (int j = 0; j < switches.firstKey(); j++) {
                JSONObject piece = bends.get(j);
                if (piece != null) {
                    bendsPiecesBeforeSwitch.add(piece);
                    calculateArcLength(piece, bendLengthBeforeSwitch, laneBendLength, laneCurvature, laneDuration);
                }
            }

            setBendInfo(bendsPiecesBeforeSwitch, bendLengthBeforeSwitch, laneBendLength, switches.lastKey(), laneCurvature, laneDuration);

            double lastRadius = 0;
            double lastAngle = 0;
            int lastBendIndex = 0;
            int staringPieceIndex = 0;
            boolean isStartingIndexInBend = false;
            for (Map.Entry<Integer, JSONObject> entry : bends.entrySet()) {
                int index = entry.getKey();
                JSONObject bend = entry.getValue();
                double radius = (Double) bend.getObject("radius", Double.valueOf(0d));
                double angle = (Double) bend.getObject("angle", Double.valueOf(0d));
                if (radius != lastRadius || angle != lastAngle || index - bends.lowerKey(index) != 1) {
                    if (index == bends.firstKey() && index == 0 && bends.get(pieces.size() - 1) != null && bends.get(pieces.size() - 1).getAsDouble("radius") == radius && bends.get(pieces.size() - 1).getAsDouble("angle") == angle) {
                        isStartingIndexInBend = true;
                    }
                    BendInfo bendInfo = new BendInfo();
                    bendInfo.radius = radius;
                    bendInfo.angle = angle;
                    bendInfo.pieceNumber++;
                    bendInfo.totalAngle += angle;
                    staringPieceIndex = index;
                    bendEnterMap.put(staringPieceIndex, bendInfo);
                  /*  for (int i = 0; i < lanes.size(); i++) {
                        double bendRadius = getCurrentLaneRadius(index, i);
                        bendInfo.maximumEnteringSpeeds.add(Math.sqrt(bendRadius * getCoherenceForSpeed(bendRadius)));
                        bendInfo.coherenceSpeeds.add(getCoherenceForSpeed(bendRadius));
                    } */
                } else {
                    BendInfo bendInfo = bendEnterMap.get(staringPieceIndex);
                    bendInfo.pieceNumber++;
                    bendInfo.totalAngle += angle;
                }

                lastRadius = radius;
                lastAngle = angle;
            }

            if (isStartingIndexInBend) {
                BendInfo bendInfo = bendEnterMap.lastEntry().getValue();
                BendInfo firstBendInfo = bendEnterMap.firstEntry().getValue();
                bendInfo.totalAngle += firstBendInfo.totalAngle;
                bendInfo.pieceNumber += firstBendInfo.pieceNumber;
                bendEnterMap.remove(bendEnterMap.firstKey());
            }

            printInfo("bend enter map " + bendEnterMap.toString());
            setCoherence();
           /* for (Map.Entry<Integer, JSONObject> entry : bends.entrySet()) {
                int index = entry.getKey();
                JSONObject bend = entry.getValue();
                double radius = (Double) bend.getObject("radius", Double.valueOf(0d));
                if (lastRadius != radius && lastRadius != 0) {
                    curvatureChangeBends.put(lastBendIndex, bend);
                }
                lastBendIndex = index;
                lastRadius = radius;
                if (index != 0 && bends.lastKey() != pieces.size() - 1 && bends.get(index - 1) == null) {
                    curvatureChangeBends.put(Integer.valueOf(index - 1), bend);
                }

                if (index == 0 && bends.lastKey() != pieces.size() - 1 && bends.get(pieces.size() - 1) == null) {
                    curvatureChangeBends.put(Integer.valueOf(pieces.size() - 1), bend);
                }

                if (index != pieces.size() - 1 && bends.firstKey() != 0 && bends.get(index + 1) == null) {
                    curvatureChangeBends.put(Integer.valueOf(index), (JSONObject) pieces.get(index + 1));
                }

                if (index == pieces.size() - 1 && bends.firstKey() != 0 && bends.get(0) == null) {
                    curvatureChangeBends.put(Integer.valueOf(pieces.size() - 1), (JSONObject) pieces.get(0));
                }
            } */

            //  printInfo("curve change " + curvatureChangeBends.toString());


            initLaneLengthInEachPiece();
            searchShortestPath(switches.firstEntry(), shortestPath);

            printInfo("Shortest Path: " + shortestPath);
        } catch (
                Exception e
                )

        {
            System.out.println(e.getMessage());
            e.printStackTrace();
        }

    }

    private void setBendInfo(List<JSONObject> bendsPiecesBeforeSwitch, Double bendLengthBeforeSwitch, List<Double> laneBendLength, int switchPieceIndex, List<Double> laneCurvature, List<Double> laneDuration) {
        //  printInfo("switch index " + switchPieceIndex);
        //   printInfo(bendsPiecesBeforeSwitch.toString());

        BendInfo bendInfo = new BendInfo();
        bendInfo.bends = new ArrayList<>(bendsPiecesBeforeSwitch);
        bendInfo.bendLengthTotal = bendLengthBeforeSwitch;
        bendInfo.laneBendLength = new ArrayList<>(laneBendLength);
        bendInfo.laneCurvatureLength = new ArrayList<>(laneCurvature);
        bendInfo.laneTimeDuration = new ArrayList<>(laneDuration);
        switchBendsMap.put(switchPieceIndex, bendInfo);

        bendsPiecesBeforeSwitch.clear();
        bendLengthBeforeSwitch = 0d;
        initLaneBendLength(laneBendLength, laneCurvature, laneDuration);
    }

    private void printBendInfo() {
        int i = 0;
        for (Map.Entry<Integer, BendInfo> entry : switchBendsMap.entrySet()) {
            printInfo("switch number " + (++i));
            printInfo("Switch piece index " + entry.getKey());
            BendInfo bendInfo = entry.getValue();
            if (bendInfo != null) {
                printInfo(entry.getValue().bends.toString());
                printInfo(entry.getValue().laneBendLength.toString());
            }

        }
    }

    private double searchMinimumCurvature(Map.Entry<Integer, JSONObject> switchPiece, TreeMap<Integer, Integer> shortestPath) {
        Double min = 100000d;
        int laneIndex = 0;
        int switchPieceIndex = switchPiece.getKey();
        BendInfo bendInfo = switchBendsMap.get(switchPieceIndex);   //get bends info for the switch piece. e.g. if there are 3 bends after this switch piece and before next switch
        if (bendInfo == null) {
            return 0d;
        }
        for (Double laneCurvature : bendInfo.laneCurvatureLength) {
            Double totalBendCurvature = laneCurvature;
            TreeMap<Integer, Integer> bestLaneNext = new TreeMap<>();
            Map.Entry<Integer, JSONObject> nextSwitchPiece = switches.higherEntry(switchPieceIndex);
            if (nextSwitchPiece != null) {
                totalBendCurvature += searchMinimumCurvature(nextSwitchPiece, bestLaneNext);
            }
            if (totalBendCurvature < min) {
                laneIndex = bendInfo.laneCurvatureLength.indexOf(laneCurvature);
                if (min != -1d) {
                    shortestPath.clear();
                    shortestPath.putAll(bestLaneNext);
                    shortestPath.put(switchPieceIndex, laneIndex);
                } else {
                    shortestPath.putAll(bestLaneNext);
                    shortestPath.put(switchPieceIndex, laneIndex);
                }
                min = totalBendCurvature;
            }
        }

        return min;
    }

    private void printInfo(String message) {
        System.out.println(message);
    }


    private double searchMinimumTime(Map.Entry<Integer, JSONObject> switchPiece, TreeMap<Integer, Integer> shortestPath) {
        Double min = 100000d;
        int laneIndex = 0;
        int switchPieceIndex = switchPiece.getKey();
        BendInfo bendInfo = switchBendsMap.get(switchPieceIndex);   //get bends info for the switch piece. e.g. if there are 3 bends after this switch piece and before next switch
        if (bendInfo == null) {
            return 0d;
        }
        for (Double laneTime : bendInfo.laneTimeDuration) {
            Double totalBendTime = laneTime;
            TreeMap<Integer, Integer> bestLaneNext = new TreeMap<>();
            Map.Entry<Integer, JSONObject> nextSwitchPiece = switches.higherEntry(switchPieceIndex);
            if (nextSwitchPiece != null) {
                totalBendTime += searchMinimumTime(nextSwitchPiece, bestLaneNext);
            }
            if (totalBendTime < min) {
                laneIndex = bendInfo.laneTimeDuration.indexOf(laneTime);
                if (min != -1d) {
                    shortestPath.clear();
                    shortestPath.putAll(bestLaneNext);
                    shortestPath.put(switchPieceIndex, laneIndex);
                } else {
                    shortestPath.putAll(bestLaneNext);
                    shortestPath.put(switchPieceIndex, laneIndex);
                }
                min = totalBendTime;
            }
        }

        return min;
    }

    private double searchShortestPath(Map.Entry<Integer, JSONObject> switchPiece, TreeMap<Integer, Integer> shortestPath) {
        Double min = 100000d;
        int laneIndex = 0;
        int switchPieceIndex = switchPiece.getKey();
        //   printInfo("switch index " + switchPieceIndex);
        BendInfo bendInfo = switchBendsMap.get(switchPieceIndex);   //get bends info for the switch piece. e.g. if there are 3 bends after this switch piece and before next switch

        if (bendInfo == null) {
            return 0;
        }

        //  printInfo(switchBendsMap.toString());

        for (Double laneBendLength : bendInfo.laneBendLength) {  //iterator each lane length in bends
            Double totalBendLength = laneBendLength;

            TreeMap<Integer, Integer> bestLaneNext = new TreeMap<>();
            Map.Entry<Integer, JSONObject> nextSwitchPiece = switches.higherEntry(switchPieceIndex);  //get next switch piece
            if (nextSwitchPiece != null) {
                totalBendLength += searchShortestPath(nextSwitchPiece, bestLaneNext); //search the shortest in next switch piece
            }
            if (totalBendLength == 0) {
                min = 0d;
                if (nextSwitchPiece != null) {
                    shortestPath.putAll(bestLaneNext);
                }
                Integer nextBendIndex = bends.ceilingKey(switchPieceIndex);
                if (nextBendIndex == null) {
                    nextBendIndex = bends.firstKey();
                }
                if ((Double) bends.get(nextBendIndex).getObject("angle", Double.valueOf(0d)) > 0) {
                    shortestPath.put(switchPieceIndex, numberOfLane - 1);
                } else {
                    shortestPath.put(switchPieceIndex, 0);
                }
                break;
            }
            if (totalBendLength < min) {     //if it is less than befor
                laneIndex = bendInfo.laneBendLength.indexOf(laneBendLength);  //find the laneIndex
                if (min != 100000d) {         //it has been search once
                    shortestPath.clear();
                    shortestPath.putAll(bestLaneNext);
                    shortestPath.put(switchPieceIndex, laneIndex);
                } else {
                    shortestPath.putAll(bestLaneNext);
                    shortestPath.put(switchPieceIndex, laneIndex);
                }
                min = totalBendLength;
            }
        }

        return min;
    }

    private void printLaneLengthTotal() {
        double laneLength[] = new double[lanes.size()];
        for (Map.Entry<Integer, Map<Integer, Double>> entry : pieceLaneLengthMap.entrySet()) {
            Map<Integer, Double> laneLengthMap = entry.getValue();
            for (Map.Entry<Integer, Double> mapEntry : laneLengthMap.entrySet()) {
                laneLength[mapEntry.getKey()] += mapEntry.getValue();
            }
        }

        for (int i = 0; i < laneLength.length; i++) {
            printInfo("lane " + i + " length " + laneLength[i]);
        }

        for (Map.Entry<Integer, Double> entry : lapLength.entrySet()) {
            printInfo("Lap " + entry.getKey() + " length " + entry.getValue());
        }


    }


    private void calculateArcLength(JSONObject json, Double bendLengthBeforeSwitch, List<Double> laneBendLength, List<Double> laneCurvature, List<Double> timeDuration) {
        if (json == null) {
            bendLengthBeforeSwitch = 0d;
            for (int k = 0; k < numberOfLane; k++) {
                laneBendLength.add(0d);
                laneCurvature.add(0d);
                timeDuration.add(0d);
            }
            return;
        }

        double radius = json.getAsDouble("radius");
        double angle = json.getAsDouble("angle");
        double arcLength = Math.abs(angle) * radius * Math.PI / 180;
        bendLengthBeforeSwitch += arcLength;

        for (int i = 0; i < numberOfLane; i++) {
            JSONObject lane = (JSONObject) lanes.get(i);
            double distanceFromCenter = lane.getAsDouble("distanceFromCenter");
            double ratio;
            double curvature;
            double duration;
            if (angle > 0) {
                ratio = (radius - distanceFromCenter) / radius;
                curvature = 1 / (radius - distanceFromCenter);
                duration = (radius - distanceFromCenter) / 5;
            } else {
                curvature = 1 / (radius + distanceFromCenter);
                ratio = (radius + distanceFromCenter) / radius;
                duration = (radius + distanceFromCenter) / 6;
            }

            Double laneXLength = laneBendLength.get(i);
            Double curvatureInLane = laneCurvature.get(i);
            Double durationInLane = timeDuration.get(i);
            curvatureInLane += curvature;
            laneXLength += arcLength * ratio;
            durationInLane += duration;

            laneBendLength.set(i, laneXLength);
            laneCurvature.set(i, curvatureInLane);
            timeDuration.set(i, durationInLane);
        }
    }


}